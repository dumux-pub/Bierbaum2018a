# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git reset --hard d3f0f1385c956f24d50cebd91daf107e65a7886c
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git reset --hard 08fa18a9a99f05b1cd9b79c7c6b27b9319b2c2b8
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git reset --hard 7ab57302d969c11d55a2ee4360ce35434078fa2e
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git reset --hard a115c1befe934de08c0e17fb13dcfccff9227e5f
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git reset --hard 34a95d2ea6e9b6b025282cb6c5e01fd554196971
cd ..

# dune-foamgrid
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git reset --hard 5107b112f11f100744fb0d475bee94c84819b8a2
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git reset --hard 647137ea6b4022cd397d7c94e7827ae9f4ca904c
cd ..

# dumux-mixeddimension
git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-mixeddimension.git
cd dumux-mixeddimension
git reset --hard 16ed99d17d31d01d580791cd93dae26a94c68ed3
cd ..

# Bierbaum2018a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Bierbaum2018a.git
cd ..
