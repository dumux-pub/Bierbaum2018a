import math
import numpy as np


# the grid
vertices = []
throats = []
bodyRadius = []
throatRadius = []
throatLength = []
inletTmp = []
outletTmp = []

#read the pore locations and store them in array
with open('pore_loc.txt') as fp:
    for line in fp:
        #print line
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        vertices.append(collapsedSplit)

#read the pore body radii and store them in array
with open('r_body.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        bodyRadius.append(collapsedSplit)

#read the inlet pore information and store it in array
with open('inlet.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        collapsedSplit = map(int, collapsedSplit) #convert string to int
        collapsedSplit = [x-1 for x in collapsedSplit] #remove offset
        inletTmp.append(collapsedSplit)

#read the inlet pore information and store it in array
with open('outlet.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        collapsedSplit = map(int, collapsedSplit) #convert string to int
        collapsedSplit = [x-1 for x in collapsedSplit] #remove offset
        outletTmp.append(collapsedSplit)


#array "boundaries" has -1 / 1 at inlet / outlet pore bodies and 0s everywhere else
boundaries = np.zeros(len(vertices))


for x in inletTmp:
    boundaries[x] = -1

for x in outletTmp:
    boundaries[x] = 1

#add pore body radius to array of vertices
vertices = [x + [bodyRadius[idx][1]] + [str(boundaries[idx])]  for idx, x in enumerate (vertices)]


#read the throat connections data and store them in array
with open('con_throat.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        del collapsedSplit[0] #remove unneeded numbering
        collapsedSplit = map(int, collapsedSplit) #convert string to int
        collapsedSplit = [x-1 for x in collapsedSplit] #remove offset
        collapsedSplit = map(str, collapsedSplit) #convert back to string
        throats.append(collapsedSplit)

#read the throat radii and store them in array
with open('r_throat.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        throatRadius.append(collapsedSplit)

#read the throat length and store them in array
with open('l_throat.txt') as fp:
    for line in fp:
        collapsed = ' '.join(line.split())
        collapsedSplit = collapsed.split(' ')
        throatLength.append(collapsedSplit)

#add throat radius and lenght to array of throats
throats = [x + throatRadius[idx] + throatLength[idx]  for idx, x in enumerate (throats)]

#write everything to DGF file
gridDGF = open('pnm_converted.dgf', 'w')
gridDGF.write('DGF\nVertex % Coordinates and volumes of the pore bodies\nparameters 2\n')

for vertex in vertices:
    gridDGF.write(" ".join(vertex) + "\n")

gridDGF.write('#\nSIMPLEX % Connections of the pore bodies (pore throats)\nparameters 2\n')

for throat in throats:
    gridDGF.write(" ".join(throat) + "\n")

gridDGF.write('#\n')