// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test for the pore network model
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>     //for .txt outputs

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/grid/porenetworkgridcreator.hh>
#include "drainageproblem_2.hh"
#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>
#include <dumux/porenetworkflow/2p/newtonsolver.hh>


int main(int argc, char** argv)
{
    using namespace Dumux;

    using TypeTag = TTAG(DrainageProblem);

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    ////////////////////////////////////////////////////////////
    // parse the command line arguments and input file
    ////////////////////////////////////////////////////////////

    // parse command line arguments
    Parameters::init(argc, argv);

    /////////////////////////////////////////////////////////////////////
    // try to create a grid (from the given grid file or the input file)
    /////////////////////////////////////////////////////////////////////

    using GridManager = Dumux::PoreNetworkGridCreator<3>;
    GridManager gridManager;
    gridManager.init();

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the spatial parameters
    using SpatialParams = typename GET_PROP_TYPE(TypeTag, SpatialParams);
    auto spatialParams = std::make_shared<SpatialParams>(fvGridGeometry, gridManager.getGridData());

    // the problem (boundary conditions)
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    auto problem = std::make_shared<Problem>(fvGridGeometry, spatialParams);

    // the solution vector
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    SolutionVector x(leafGridView.size(GridView::dimension));
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x, xOld);

    // get some time loop parameters
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    const bool stepWiseDrainage = getParam<int>("Problem.NumSteps") > 1;    //stepwise drainage or constant boundary conditions

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");
    
    // intialize the vtk output module
    using VtkOutputFields = typename GET_PROP_TYPE(TypeTag, VtkOutputFields);
    PNMVtkOutputModule<TypeTag> vtkWriter(*gridVariables, x, problem->name());
    VtkOutputFields::init(vtkWriter); //! Add model specific output fields
    std::vector<double> swMin(leafGridView.size(GridView::dimension));
    std::vector<double> pcAvg(leafGridView.size(GridView::dimension));
    std::vector<double> deltaT(leafGridView.size(GridView::dimension));
    vtkWriter.addField(swMin, "swMin", decltype(vtkWriter)::FieldType::vertex);
    vtkWriter.addField(pcAvg, "pcAvg", decltype(vtkWriter)::FieldType::vertex);
    vtkWriter.addField(deltaT, "deltaT", decltype(vtkWriter)::FieldType::vertex);

    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver
    // using LinearSolver = SSORCGBackend;//AMGBackend<TypeTag>;
    using LinearSolver = UMFPackBackend;//AMGBackend<TypeTag>;
    // auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->elementMapper());
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = PNMNewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    Scalar timeOld;
    Scalar timeStep;
    std::vector<Scalar> dtSizes, t;
    
    auto getSwMin = [&](auto& swMin, const Scalar pcMin)
    {
        std::vector<bool> visited(swMin.size(), false);
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        auto fvGeometry = localView(*fvGridGeometry);

        for(const auto& element : elements(fvGridGeometry->gridView()))
        {
            fvGeometry.bindElement(element);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto vIdx = scv.dofIndex();
                if(visited[vIdx])
                    continue;

                visited[vIdx] = true;

                const auto& materialParams = problem->spatialParams().materialLawParams(element, scv, 0);
                swMin[vIdx] = MaterialLaw::swMin(materialParams, materialParams.poreRadius(), pcMin);
            }
        }
    };

    auto getPcAvg = [&](auto& pcAvg)  //averaging of local capillary pressures
    {
        Scalar sumAn = 0;
        Scalar sumPcAn = 0;
        Scalar sumPc = 0;
        Scalar Req;

        auto fvGeometry = localView(*fvGridGeometry);
        auto curElemVolVars = localView(gridVariables->curGridVolVars());
        std::vector<bool> vertexMarker(fvGridGeometry->numDofs(), false);

        for(const auto& element : elements(fvGridGeometry->gridView()))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, x);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto vIdx = scv.dofIndex();
                const auto& materialParams = problem->spatialParams().materialLawParams(element, scv, 0);
                const auto spatialParams = problem->spatialParams();
                Scalar Sw = 1-x[vIdx][1];
                const Scalar sigma = materialParams.sigma();
                Scalar poreRadius = materialParams.poreRadius();
                const auto& volVars = curElemVolVars[scv];
                const Scalar pc = volVars.capillaryPressure();

                if(vertexMarker[vIdx] == true)
                {
                    //main terminal menisci
                    if(!curElemVolVars.gridVolVars().invaded(element) && Sw < 0.48)
                    {
                        Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                        Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                        Scalar An_menisci;
                        if(rootTerm > 0)  //negative value if sw < swMin!!
                            An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                        else
                        {
                            An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);
                        }

                        sumAn = sumAn + An_menisci;
                        sumPcAn = sumPcAn + pc * An_menisci;
                    }
                    continue;
                }

                if(Sw < 0.48)
                {
                    Req = poreRadius * (1-std::exp(-6.83 * Sw));
                    Scalar An = 4*M_PI * Req*Req + 6*M_PI * Req * (poreRadius - Req);
                    sumAn = sumAn + An;
                    sumPcAn = sumPcAn + pc * An;
                    sumPc = sumPc + pc;

                    //main terminal menisci
                    if(!curElemVolVars.gridVolVars().invaded(element))
                    {
                        Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                        Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                        Scalar An_menisci;
                        if(rootTerm > 0)  //negative value if sw < swMin!!
                            An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                        else
                        {
                            An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);
                        }

                        sumAn = sumAn + An_menisci;
                        sumPcAn = sumPcAn + pc * An_menisci;
                    }
                }
                else
                {
                    Req = poreRadius * std::cbrt((6/M_PI) * (1-Sw));
                    Scalar An = 4*M_PI * Req*Req;
                    sumAn = sumAn + An;
                    sumPcAn = sumPcAn + pc * An;
                    sumPc = sumPc + pc;
                }
                vertexMarker[vIdx] = true;
            }
        }
        Scalar pcAvg_ = sumPcAn/sumAn;
        for(int i=0; i!=fvGridGeometry->numDofs(); i++)
            pcAvg[i] = pcAvg_;
    };

    // time loop
    timeLoop->start(); do
    {
        timeOld = timeLoop->time();

        if(problem->simulationFinished())
            timeLoop->setFinished();

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // try solving the non-linear system
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        //get current swMin
        Scalar pcMin = gridVariables->curGridVolVars().pcMin(x);
        getSwMin(swMin, pcMin);
        getPcAvg(pcAvg);
        
        // get time step sizes
        timeStep = timeLoop->time() - timeOld;
        t.push_back(timeLoop->time());
        dtSizes.push_back(timeStep);
        for(int i=0; i!=deltaT.size(); i++)
            deltaT[i] = timeStep;

        // write vtk output
        if(problem->shouldWriteOutput(timeLoop->timeStepIndex(), *gridVariables))
            vtkWriter.write(timeLoop->time());

        problem->P_Avg(*gridVariables, x);  //averaging of phase pressures
        problem->postTimeStep(*gridVariables, x, timeLoop->time(), timeStep);  //logfile; increase pc at inlet

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());
    
    nonLinearSolver.report();

    //plot the pc-S curve, if desired
#ifdef HAVE_GNUPLOT
    if(stepWiseDrainage)
        problem->plotPcS();
#endif

    GnuplotInterface<Scalar> gnuplot_;
    //dt
//     gnuplot_.setYRange(1e-5, 2.5e-4);
//     gnuplot_.setXlabel("time [s]");
//     gnuplot_.setYlabel("dt [-]");
//     gnuplot_.addDataSetToPlot(t, dtSizes, "dt");
//     gnuplot_.plot("dt");
//
//     std::ofstream myfile;
//     myfile.open ("dt.txt");
//     for(int i = 0; i != t.size(); i++)
//     {
//         myfile << t[i] << ", " << dtSizes[i] << std::endl;
//     }
//     myfile.close();
//
//     for(int i=0; i != markerElement.size(); i++)
//     {
//         std::cout << "element " << i << " invaded? --> " << markerElement[i] << std::endl;
//     }


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    timeLoop->finalize(leafGridView.comm());

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

}
