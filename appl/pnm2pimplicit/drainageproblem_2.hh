// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM2P_PROBLEM_HH
#define DUMUX_PNM2P_PROBLEM_HH

#define CUSTOMCOMPONENTS 0

#include <dune/foamgrid/foamgrid.hh>

// base problem
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/porenetworkflow/2p/model.hh>
#include <dumux/porenetworkflow/2p/gridvolumevariables.hh>

// spatial params
#include <dumux/material/spatialparams/porenetwork/myporenetwork2p.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/porenetworkflow/common/utilities.hh>

#include <dumux/material/fluidsystems/myh2oair.hh>

namespace Dumux
{
template <class TypeTag>
class DrainageProblem;

namespace Properties
{
#if ISOTHERMAL
NEW_TYPE_TAG(DrainageProblem, INHERITS_FROM(PNMTwoP));
#else
NEW_TYPE_TAG(DrainageProblem, INHERITS_FROM(PNMTwoPNI));
#endif

// Set the problem property
SET_TYPE_PROP(DrainageProblem, Problem, Dumux::DrainageProblem<TypeTag>);

SET_PROP(DrainageProblem, FluidSystem)
  {
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
#if !CUSTOMCOMPONENTS
    typedef Dumux::FluidSystems::H2OAir<Scalar, Dumux::Components::SimpleH2O<Scalar> > type;
#else
    typedef Dumux::FluidSystems::MyH2OAir<TypeTag, Scalar, Dumux::Components::SimpleH2O<Scalar> > type;
#endif
  };

// Set the grid type
SET_TYPE_PROP(DrainageProblem, Grid, Dune::FoamGrid<1, 3>);

SET_TYPE_PROP(DrainageProblem, SinglePhaseTransmissibility, TransmissibilityAzzamDullien<TypeTag>);

SET_PROP(DrainageProblem, GridVolumeVariables)
{
private:
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Traits = PNMDefaultGridVolumeVariablesTraits<Problem, VolumeVariables, Indices>;

public:
    using type = MyPNMTwoPGridVolumeVariables<Problem, VolumeVariables,
                                            GET_PROP_VALUE(TypeTag, EnableGridVolumeVariablesCache),
                                            Traits>;
};

SET_PROP(DrainageProblem, SpatialParams)
{
private:
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
public:
    using type = MyPNMTwoPSpatialParams<FVGridGeometry, Scalar, Labels,
                                      GET_PROP_VALUE(TypeTag, PoreGeometry),
                                      GET_PROP_VALUE(TypeTag, ThroatGeometry),
                                      GET_PROP_VALUE(TypeTag, ZeroPc)>;
};


// SET_INT_PROP(DrainageProblem,
//              Formulation,
//              TwoPFormulation::pwsn);

// Set the allowed discrepancy for the solution plausibility check
SET_SCALAR_PROP(DrainageProblem, CheckSolutionEpsilon, 5e-9);
}

template <class TypeTag>
class DrainageProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    // using SpatialParams = typename GET_PROP_TYPE(TypeTag, SpatialParams);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);

    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);

    using MaterialLaw = typename DrainageProblem::SpatialParams::MaterialLaw;

    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        pwIdx = Indices::pressureIdx,
        snIdx = Indices::saturationIdx,
//         pnIdx = Indices::pnIdx,
//         swIdx = Indices::swIdx,


        // phase indices
        wPhaseIdx = FluidSystem::phase0Idx,
        nPhaseIdx = FluidSystem::phase1Idx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    enum { dofCodim =  dim };

public:
    template<class SpatialParams>
    DrainageProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(fvGridGeometry, spatialParams), eps_(1e-7)
    {
        verbose_ = getParam<bool>("Problem.Verbose", false);
        VtpOutputFrequency_ = getParam<int>("Problem.VtpOutputFrequency");
        initialPc_ = getParam<Scalar>("Problem.InitialPc");
        finalPc_ = getParam<Scalar>("Problem.FinalPc");
        numSteps_ = getParam<int>("Problem.NumSteps");
        episodeLenght_ = getParam<Scalar>("Problem.EpisodeLength");
        breakthrough_ = false;
        breakthroughSw_ = 0.0;
        qnBreak_ = 0.0;

        swEquilibrium_.resize(3);
        swEquilibrium_ = {1.0, 1.0, 1.0};


        pcEpisode_.resize(numSteps_+1);
        for (int i = 0 ; i < pcEpisode_.size()-1; i++)
              pcEpisode_[i] = initialPc_ + i*(finalPc_ - initialPc_)/numSteps_;
        pcEpisode_[numSteps_] = finalPc_;

        std::cout << "The following global PCs are applied: " << std::endl;
        for (auto x: pcEpisode_)
        {
            std::cout << x << std::endl;
        }

        logfile_.open("logfile_" + this->name() + ".txt"); //for the logfile
        logfile_ <<"Logfile for: " + this->name()  << std::endl;
        logfile_ << std::left << std::setw(25) << std::setfill(' ') << std::setprecision(14) << "Time"
                 << std::left << std::setw(25) << std::setfill(' ') << "Time Step"
                 << std::left << std::setw(25) << std::setfill(' ') << "globalPc"
                 << std::left << std::setw(25) << std::setfill(' ') << "avgPc"
                 << std::left << std::setw(25) << std::setfill(' ') << "swAvg(inletIncl)"
                 << std::left << std::setw(25) << std::setfill(' ') << "swAvg(inletExcl)"
                 << std::left << std::setw(25) << std::setfill(' ') << "PnAvg"
                 << std::left << std::setw(25) << std::setfill(' ') << "PwAvg"
                 << std::left << std::setw(25) << std::setfill(' ') << "breakthroughSw"
                 << std::left << std::setw(25) << std::setfill(' ') << "qn(breakthrough)"
                 << std::endl;

        step_ = 0;
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput(const int timeStepIndex, const GridVariables& gridVariables) const //define output
    {
        if(VtpOutputFrequency_ < 0)
            return true;

        if(VtpOutputFrequency_ == 0)
            return ( timeStepIndex == 0 || gridVariables.curGridVolVars().invasionOccured() );
        else
            return ( timeStepIndex % VtpOutputFrequency_ == 0 || gridVariables.curGridVolVars().invasionOccured() );
    }


    /*!
     * \brief Called at the end of each time step
     */
    void postTimeStep(const GridVariables& gridVariables, const SolutionVector& sol, const Scalar t, const Scalar dt)
    {
        const auto avg = Averaging<TypeTag>::averagedValues(*this, gridVariables, sol);

        // store the three most recent averaged saturations
        std::rotate(swEquilibrium_.rbegin(), swEquilibrium_.rbegin()+1, swEquilibrium_.rend());
        swEquilibrium_[0]= avg.sw;

        // Check for steady state and end episode
        const Scalar change = std::abs(swEquilibrium_[0]-swEquilibrium_[2])/swEquilibrium_[0];
        const Scalar pc = pcEpisode_[step_];
        std::cout << "global pC applied: " << pc << " / " << finalPc_ << " (step " << step_ << " of " << numSteps_ << ")" << std::endl;
        std::cout << "swAverage: " << swEquilibrium_[0] << " (relative shift: " << change << ")" << std::endl;

        std::vector<bool> Vertex_done(this->fvGridGeometry().numDofs(), false);
        Scalar V_tot = 0;  //for average saturation (without inlet pores)
        Scalar V_w = 0;

        const auto& gridView = this->fvGridGeometry().gridView();
        for(const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);
            for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
            {
                const auto vIdx = scv.dofIndex();
                const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);
                const Scalar sw = 1 - sol[vIdx][1];
                if(!this->spatialParams().isInletPore(vIdx))  //calculate average saturation
                {
                    if(!Vertex_done[vIdx])
                    {
                        Vertex_done[vIdx] = true;

                        V_tot += this->spatialParams().poreVolume(vIdx);
                        V_w += sw * this->spatialParams().poreVolume(vIdx);
                    }
                }
                if(breakthrough_ == false && this->spatialParams().isOutletPore(vIdxNeighbor))  //check for breakthrough
                {
                    if(sw < 1.0)
                        breakthrough_ = true;
                }
            }
        }

        if(breakthrough_ == true)
        {
            breakthroughSw_ = V_w/V_tot;
            if(qnBreak_ == 0)
                qnBreak_ = (V_tot - V_w)/t;
        }

        //pcAvg calculation
        Scalar sumAn = 0;
        Scalar sumPcAn = 0;
        Scalar sumPc = 0;
        Scalar Req;
        Scalar pcAvg = 0;

        auto fvGeometry = localView(this->fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
        std::vector<bool> vertexMarker(this->fvGridGeometry().numDofs(), false);

        for(const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, sol);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto vIdx = scv.dofIndex();
                if(!this->spatialParams().isInletPore(vIdx))
                {
                    const auto& materialParams = this->spatialParams().materialLawParams(element, scv, 0);
                    const auto spatialParams = this->spatialParams();
                    Scalar Sw = 1-sol[vIdx][1];
                    const Scalar sigma = materialParams.sigma();
                    Scalar poreRadius = materialParams.poreRadius();
                    const auto& volVars = curElemVolVars[scv];
                    const Scalar pc = volVars.capillaryPressure();

                    if(vertexMarker[vIdx] == true)
                    {
                        //main terminal menisci
                        if(!curElemVolVars.gridVolVars().invaded(element) && Sw < 0.48)
                        {
                            Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                            Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                            Scalar An_menisci;
                            if(rootTerm > 0)  //negative value if sw < swMin!!
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                            else
                            {
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);
                            }


                            sumAn = sumAn + An_menisci;
                            sumPcAn = sumPcAn + pc * An_menisci;
                        }
                        continue;
                    }

                    if(Sw < 0.48)
                    {
                        Req = poreRadius * (1-std::exp(-6.83 * Sw));
                        Scalar An = 4*M_PI * Req*Req + 6*M_PI * Req * (poreRadius - Req);
                        sumAn = sumAn + An;
                        sumPcAn = sumPcAn + pc * An;
                        sumPc = sumPc + pc;

                        //main terminal menisci
                        if(!curElemVolVars.gridVolVars().invaded(element))
                        {
                            Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                            Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                            Scalar An_menisci;
                            if(rootTerm > 0)  //negative value if sw < swMin!!
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                            else
                            {
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);
                            }

                            sumAn = sumAn + An_menisci;
                            sumPcAn = sumPcAn + pc * An_menisci;
                        }
                    }
                    else
                    {
                        Req = poreRadius * std::cbrt((6/M_PI) * (1-Sw));
                        Scalar An = 4*M_PI * Req*Req;
                        sumAn = sumAn + An;
                        sumPcAn = sumPcAn + pc * An;
                        sumPc = sumPc + pc;
                    }
                    vertexMarker[vIdx] = true;
                }
            }
        }
        if(sumAn > 0.0)
            pcAvg = sumPcAn/sumAn;

        if(change < 1e-8)  //next pc step if equilibrium state is reached
        {
            ++step_;
            // remember swEquilibrium and pc_global
            sw_Avg.push_back(V_w/V_tot);
            pc_Avg.push_back(pcAvg);
        }

        logfile_ << std::fixed << std::left << std::setw(25) << std::setfill(' ') << t
                               << std::left << std::setw(25) << std::setfill(' ') << dt
                               << std::left << std::setw(25) << std::setfill(' ') << pc
                               << std::left << std::setw(25) << std::setfill(' ') << pcAvg
                               << std::left << std::setw(25) << std::setfill(' ') << avg.sw
                               << std::left << std::setw(25) << std::setfill(' ') << V_w/V_tot
                               << std::left << std::setw(25) << std::setfill(' ') << Pn_Avg_now
                               << std::left << std::setw(25) << std::setfill(' ') << Pw_Avg_now
                               << std::left << std::setw(25) << std::setfill(' ') << breakthroughSw_
                               << std::left << std::setw(25) << std::setfill(' ') << qnBreak_
                               << std::endl;
    }

    void P_Avg(const GridVariables& gridVariables, SolutionVector& sol)  //averaging of phase pressures
    {
        Scalar sum_pwswV = 0.0;
        Scalar sum_pnsnV = 0.0;
        Scalar sum_swV = 0.0;
        Scalar sum_snV = 0.0;
        const auto& gridView = this->fvGridGeometry().gridView();
        auto fvGeometry = localView(this->fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());

        std::vector<bool> Vertex_done(this->fvGridGeometry().numDofs(), false);

        for(const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, sol);
            for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
            {
                const auto vIdx = scv.dofIndex();
                if(!Vertex_done[vIdx] == true)
                {
                    Vertex_done[vIdx] = true;
                    if(!this->spatialParams().isInletPore(vIdx))
                    {
                        const auto spatialParams = this->spatialParams();
                        const auto& volVars = curElemVolVars[scv];
                        const Scalar poreVolume = spatialParams.poreVolume(vIdx);
                        const Scalar sw = 1-sol[vIdx][1];
                        const Scalar sn = 1-sw;
                        const Scalar pc = volVars.capillaryPressure();//MaterialLaw::sw(materialParams, poreRadius, sw);
                        const Scalar pw = sol[vIdx][0];
                        const Scalar pn = pw + pc;

                        sum_pwswV += pw * sw * poreVolume;
                        sum_pnsnV += pn * sn * poreVolume;
                        sum_swV += sw * poreVolume;
                        sum_snV += sn * poreVolume;
                    }
                }
            }
        }
        Pw_Avg_now = sum_pwswV / sum_swV;

        if(sum_snV > 0.0)
            Pn_Avg_now = sum_pnsnV / sum_snV;
        else
            Pn_Avg_now = sum_pwswV / sum_swV;
    }

#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}
#endif
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if(this->spatialParams().isInletPore(scv) || this->spatialParams().isOutletPore(scv))
           bcTypes.setAllDirichlet();
//         else  //for dirichlet condition at pore adjacent to inlet pores (const flux)
//         {
//             const auto& gridView = this->fvGridGeometry().gridView();
//             const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);
//             if(this->spatialParams().isInletPore(vIdxNeighbor))
//                 bcTypes.setAllDirichlet();
            else // neuman for the remaining boundaries
                bcTypes.setAllNeumann();
//         }

#if !ISOTHERMAL
        bcTypes.setDirichlet(temperatureIdx);
#endif
        return bcTypes;
    }

    /*!
    * \brief Evaluate the boundary conditions for a dirichlet
    *        control volume.
    *
    * \param values The dirichlet values for the primary variables
    * \param vertex The vertex (pore body) for which the condition is evaluated
    *
    * For this method, the \a values parameter stores primary variables.
    */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        values[pwIdx] = 1e5;

        if (this->spatialParams().isOutletPore(scv))
            values[snIdx] = 0.0;
        else if (this->spatialParams().isInletPore(scv))
        {
            const Scalar pc = pcEpisode_[step_];
            const auto& materialParams = this->spatialParams().materialLawParams(element, scv, 0);
            const Scalar poreRadius = materialParams.poreRadius();
            Scalar sw = MaterialLaw::sw(materialParams, poreRadius, pc);
            values[snIdx] = 1-sw;
//             values[pwIdx] = 1.05e5 - pc;//1.0354e5;
//             values[snIdx] = 1.0 - 0.0650706;
        }

#if !ISOTHERMAL
        if(this->spatialParams().isInletPore(scv))
            values[temperatureIdx] = 273.15 + 15;
        else
            values[temperatureIdx] = 273.15 + 10;
#endif
        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        vertex
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * units of \f$ [ \textnormal{unit of conserved quantity} / s] \f$
     * \param vertex The vertex
     * \param volVars All volume variables for the pore
     *
     * Positive values mean that mass is created,
     * negative ones mean that it vanishes.
     */
    PrimaryVariables source(const Vertex& vertex, const VolumeVariables& volVars) const
    {
        PrimaryVariables values(0.0);
        const int vIdx =  this->fvGridGeometry().vertexMapper().index(vertex);

        if(this->spatialParams().isInletPore(vIdx))
        {
            const Scalar pc = pcEpisode_[step_];
            values[snIdx] = (volVars.pressure(nPhaseIdx) - (1e5 + pc)) * 1e8;
        }
        return values;
    }
    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);
        values[pwIdx] = 1e5;

        //get global index of pore
        int dofIdxGlobal = this->fvGridGeometry().vertexMapper().index(vertex);

        if(this->spatialParams().isInletPore(dofIdxGlobal))
        {
            values[snIdx] = 0.0;
            values[pwIdx] = 1e5;
        }
        else
            values[snIdx] = 0.0;

#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 10;
#endif
        return values;
    }

    /*!
     * \brief Evaluate the initial invasion state of a pore throat
     *
     * Returns true for a invaded throat and false elsewise.
     */
    bool initialInvasionState(const Element &element) const
    { 
        
//         return false; 
        return this->spatialParams().isInletThroat(element);
        
    }

    // \}

    /*!
     * \brief Calls gnuplot and plots the pc-S curve
     */
    void plotPcS()
    {
        std::FILE * pipe_;
        pipe_ = popen("gnuplot -persist", "w");
        std::string command = "set xrange [0:1] \n";
        command += "set xlabel 'S_w' \n";
        command += "set ylabel 'p_c' \n";
        std::string filename = "'logfile_"+ this->name() +".txt'";
        command += "plot " + filename + " using 4:3 with lines title 'p_c(S_w)'";
        fputs((command + "\n").c_str(), pipe_);
        pclose(pipe_);

        std::ofstream myfile2;  //write pcsw values on txt file
        myfile2.open ("pcsw.txt");
        myfile2 << "sw, pc" << std::endl;
        for(int i = 0; i != step_; i++)
        {
            myfile2 << sw_Avg[i] << ", " << pc_Avg[i] << std::endl;
        }
        myfile2.close();
    }


    const bool verbose() const
    { return verbose_; }

    bool simulationFinished() const
    { return step_ >= numSteps_ ; }

private:
    Scalar eps_;
    bool verbose_;
    int VtpOutputFrequency_;
    Scalar initialPc_;
    Scalar finalPc_;
    int numSteps_;
    Scalar episodeLenght_;
    std::vector<Scalar> pcEpisode_;
    std::vector<Scalar> swEquilibrium_;
    Scalar Pn_Avg_now;
    Scalar Pw_Avg_now;
    bool breakthrough_;
    Scalar breakthroughSw_;
    Scalar qnBreak_;
    std::ofstream logfile_;

    int step_;

    std::vector<Scalar> sw_Avg;
    std::vector<Scalar> pc_global;
    std::vector<Scalar> pc_Avg;
};
} //end namespace

#endif
