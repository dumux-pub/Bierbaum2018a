// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief Pressure Solver; solves pressure field in two-phase pore-network model
 */ 

#ifndef DUMUX_PNM_PRESSURESOLVER_HH
#define DUMUX_PNM_PRESSURESOLVER_HH

#include <ctime>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

#include <dune/istl/bvector.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/porenetworkflow/common/advection.hh>

namespace Dumux{
/*!
 * \file
 * \ingroup Common
 * \brief Pressure Solver; solves pressure field in two-phase pore-network model
 * \tparam 
 */

using Scalar = double;
using TypeTag2 = TTAG(PNMOnePProblemTypeTag);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MapMaker
{
public:
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;

    template<class FVGridGeometry>
    VertexMap createMap(const FVGridGeometry &fvGridGeometry, std::vector<int> &vertexIndices, std::vector<int> &elementIndices)
    {
        VertexMap map(fvGridGeometry.numDofs());

        const auto& gridView = fvGridGeometry.gridView();

        for(const auto& element : elements(gridView))
        {
            const auto eIdx = fvGridGeometry.elementMapper().index(element);      // eIdx = Index of considered element
            elementIndices[eIdx] = eIdx;     //vector gets value of eIdx at position eIdx

            const auto vIdx0 = gridView.indexSet().subIndex(element, 0, 1);      //vIdx0 gets index of first vertex, connected to considered element; idxSet.subIndex(e,i,c); // index of subentity; (reference to codimension, number subentity of e within the codimension, codimensionof the subentity)
            const auto vIdx1 = gridView.indexSet().subIndex(element, 1, 1);      //vIdx1 gets index of second vertex, connected to considered element;

            vertexIndices[vIdx0] = vIdx0;                     //vertexIndices gets value of vertex vIdx0 at position vIdx0
            vertexIndices[vIdx1] = vIdx1;                     //vertexIndices gets value of vertex vIdx1 at position vIdx1

            auto mapEntry0 = std::make_pair(vIdx1, eIdx);     //pair mapEntry0 gets values (index of second vertex, index of considered element)
            map[vIdx0].push_back(mapEntry0);                  //pushback in map at position of vertex 1

            auto mapEntry1 = std::make_pair(vIdx0, eIdx);     //pair mapEntry1 erhält Einträge (index of first vertex, index of considered element)
            map[vIdx1].push_back(mapEntry1);                  //pushback in map at position of vertex 2

        }
        return map;
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ConnectionWriter
{
public:
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;

    void connectionWriter(VertexMap& map)
    {
        const auto numVertices = map.size();

        for(int vertexIdx = 0; vertexIdx < numVertices; ++vertexIdx)      //iteration through map, i.e. vertices
        {
            std::vector<int> neighbourVertices;       //initialize vector for neighbour vertices
            std::vector<int> neighbourElements;       //initialize vector for "neighbour" elements

            std::cout << "Vertex " << vertexIdx << " has the neighbour vertices: " << std::endl;

            for(const auto& neighborData : map[vertexIdx])        //iteration through all the connections of the considered vertex
            {
                neighbourVertices.push_back(neighborData.first);                   //gets neighbour vertices
                neighbourElements.push_back(neighborData.second);                  //gets connecting elements
            }
            for(int i = 0; i!=neighbourVertices.size(); i++)
                std::cout << neighbourVertices[i] << ", " << std::endl;     //write out neighbour vertices

            std::cout << "via elements: " << std::endl;

            for(int i = 0; i!=neighbourElements.size(); i++)
                std::cout << neighbourElements[i] << ", " << std::endl;     //write out neighbour elements

            neighbourVertices.clear();       //clear vector for next vertex
            neighbourElements.clear();       //clear vector for next vertex
        }
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MatrixPattern
{
public:
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;

    template<class Problem>
    void matrixPattern(const Problem& problem, Dune::MatrixIndexSet& occupationPattern, VertexMap& map)   //matrix pattern
    {
        const auto numVertices = map.size();

        for(int vertexIdx = 0; vertexIdx < numVertices; ++vertexIdx)      //iteration through map, i.e. vertices
        {
            for(const auto& neighborData : map[vertexIdx])        //iteration through all the connections of the considered vertex
            {
                occupationPattern.add(neighborData.first, vertexIdx);                //gets positive conductivity later
                occupationPattern.add(neighborData.first, neighborData.first);             //gets negative conductivity later

                const auto& bcTypes = problem.boundaryTypes(vertexIdx);

                if(bcTypes.isDirichlet(0))                                            //for dirichlet input
                {
                    for(int n = 0; n < numVertices; n++)
                        occupationPattern.add(vertexIdx, n);
                }
                if(getParam<bool>("Problem.ConstFlux"))                               //for const flux with pseudo pore at inlet
                {
                    if(vertexIdx == 1)
                    {
                        for(int n = 0; n < numVertices; n++)
                            occupationPattern.add(vertexIdx, n);
                    }
                }
            }
        }
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class InitParams
{
public:
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    template<class Problem, class FVGridGeometry, class SolutionVector>
    void initParams(const Problem& problem, const FVGridGeometry &fvGridGeometry, SolutionVector& curSol, const Scalar& pc_inlet, xVector& p, const Scalar& Sn_initial, xVector& Sn_init, xVector& pc)   //initialize pressure field and local saturations
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto fvGeometry = localView(problem.fvGridGeometry());
        const int numVertices = fvGridGeometry.numDofs();
        std::vector<bool> Vertex_done(numVertices, false);
        const auto& gridView = fvGridGeometry.gridView();
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;
                            if(problem.spatialParams().isInletPore(vIdx))
                            {
                                const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                const Scalar poreRadius = materialParams.poreRadius();
                                Scalar sw = MaterialLaw::sw(materialParams, poreRadius, pc_inlet);   //sw is one if pc < pcHighSw
                                curSol[vIdx][0] = problem.dirichlet(vIdx)[0] - pc_inlet;
                                Scalar pw = curSol[vIdx][0];
                                Sn_init[vIdx] = 1 - sw;
                                curSol[vIdx][1] = Sn_init[vIdx];
                                p[vIdx] = sw * pw + (1-sw) * (pw + pc_inlet);
                                pc[vIdx] = pc_inlet;
                            }
                            else
                            {
                                curSol[vIdx][0] = problem.dirichlet(vIdx)[0];
//                                 Scalar pw = curSol[vIdx][0];
                                Sn_init[vIdx] = Sn_initial;
                                curSol[vIdx][1] = Sn_init[vIdx];
                                p[vIdx] = problem.dirichlet(vIdx)[0];  //sw * pw + (1-sw) * (pw + 0);   //pc=0
                                pc[vIdx] = 0.0;
                            }
                        }
                    }
                }
            }
        }
    }

    //if sw at inlet pores should correspond to pcEntry of inlet throats
    template<class Problem, class FVGridGeometry, class GridVariables, class SolutionVector>
    void initParams_test(const Problem& problem, const FVGridGeometry &fvGridGeometry, const GridVariables& gridVariables, SolutionVector& curSol, const Scalar& pn_inlet, xVector& p, const Scalar& Sn_initial, xVector& Sn_init)
    {
        const Scalar eps = 1e-2;
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
        const int numVertices = fvGridGeometry.numDofs();
        std::vector<bool> Vertex_done(numVertices, false);
        const auto& gridView = fvGridGeometry.gridView();
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary()) //necessary???
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();

                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;
                            const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                            const Scalar poreRadius = materialParams.poreRadius();
                            curElemVolVars.bind(element, fvGeometry, curSol);
                            const Scalar throatRadius = problem.spatialParams().throatRadius(element, curElemVolVars);
                            Scalar pcEntry = MaterialLaw::pcEntry(materialParams, throatRadius);
                            std::cout << "pcEntry: " << pcEntry << std::endl;

                            if(problem.spatialParams().isInletPore(vIdx))
                            {
                                Scalar sw = MaterialLaw::sw(materialParams, poreRadius, pcEntry+eps);
                                curSol[vIdx][0] = problem.dirichlet(vIdx)[0];
                                Scalar pw = curSol[vIdx][0];
                                Sn_init[vIdx] = 1 - sw;
                                curSol[vIdx][1] = Sn_init[vIdx];
                                p[vIdx] = sw * pw + (1-sw) * (pw + (pcEntry+eps));   //(pcEntry+eps) = pc
                            }
                            else
                            {
                                Scalar sw = MaterialLaw::sw(materialParams, poreRadius, pcEntry+eps);
                                curSol[vIdx][0] = problem.dirichlet(vIdx)[0];
                                Scalar pw = curSol[vIdx][0];
                                Sn_init[vIdx] = Sn_initial;
                                curSol[vIdx][1] = Sn_init[vIdx];
                                p[vIdx] = sw * pw + (1-sw) * (pw + 0);   //pc=0
                            }
                        }
                    }
                }
            }
        }
    }

    template<class Problem, class FVGridGeometry, class SolutionVector>
    void boundaryParams(const Problem& problem, const FVGridGeometry &fvGridGeometry, SolutionVector& curSol, xVector& pc, xVector& Sn_init)   //apply inlet conditions
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        const auto& gridView = fvGridGeometry.gridView();
        for(const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(problem.fvGridGeometry());
            fvGeometry.bindElement(element);
            for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
            {
                const auto vIdx = scv.dofIndex();
                if(problem.spatialParams().isInletPore(vIdx))
                {
                    const Scalar pc_inlet = pc[vIdx];
                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                    const Scalar poreRadius = materialParams.poreRadius();
                    Scalar sw = MaterialLaw::sw(materialParams, poreRadius, pc_inlet);
                    Sn_init[vIdx] = 1 - sw;
                    curSol[vIdx][1] = 1 - sw;
                }
            }
        }
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EquationSystem
{
public:

    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    template<class Matrix, class SolutionVector, class Problem>
    void fillMatrix(const Problem& problem, const SolutionVector& curSol, Matrix& M, xVector& rhs, const Scalar& Kw, const Scalar& Kn, const Scalar& pcSelf, const Scalar& pcNeighbor, const auto& vIdxSelf, const auto& vIdxNeighbor, const Scalar pn_inlet, xVector& pc)
    {
        const auto& numVertices = rhs.size();
        const auto& bcTypes = problem.boundaryTypes(vIdxSelf);
        const Scalar swSelf = 1-curSol[vIdxSelf][1];
        const Scalar snSelf = 1-swSelf;
        const Scalar swNeighbor = 1-curSol[vIdxNeighbor][1];
        const Scalar snNeighbor = 1-swNeighbor;
        Scalar pw_dir, pn_dir;

        if(bcTypes.isDirichlet(0))
        {
            if(problem.spatialParams().isInletPore(vIdxSelf))
            {
                pn_dir = problem.dirichlet(vIdxSelf)[0];   //pn at inlet
                if(!getParam<bool>("Problem.ConstFlux"))  //dirichlet condition
                    rhs[vIdxSelf] = pn_dir - swSelf * pcSelf;
                else
                    rhs[vIdxSelf] = pn_inlet - (1-snSelf) * pcSelf;  //for const flux at inlet with variable pn
            }
            else
            {
                pw_dir = problem.dirichlet(vIdxSelf)[0];   //pw at outlet
                rhs[vIdxSelf] = pw_dir;
            }

            for(int i = 0; i < numVertices; ++i)  //adjust coefficient matrix: 1 at main diagonal, rest 0
                M[vIdxSelf][i] = 0.0;
            M[vIdxSelf][vIdxSelf] = 1.0;
        }
        else
        {
            M[vIdxSelf][vIdxSelf] += (Kw + Kn); // diagonal entry          //add conductivities at correct position
            M[vIdxSelf][vIdxNeighbor] += -(Kw + Kn);                       //wrong: = -(Kw + Kn); does not matter...

            rhs[vIdxSelf] -= ((Kn * swSelf - Kw * snSelf) * pcSelf + (Kw * snNeighbor - Kn * swNeighbor) * pcNeighbor);
        }
        if(getParam<bool>("Problem.ConstFlux"))
        {
            if(vIdxSelf == 1)  //for const flux with pseudo pore at inlet
            {
                rhs[vIdxSelf] = 1.05e5 - (1-snSelf) * pcSelf;
                for(int i = 0; i < numVertices; ++i)
                    M[vIdxSelf][i] = 0.0;
                M[vIdxSelf][vIdxSelf] = 1.0;
            }
        }
    }

    template<class Problem, class FVGridGeometry, class GridVariables, class SolutionVector, class Matrix>
    void assembleSystem(const Problem& problem, const FVGridGeometry &fvGridGeometry, const GridVariables& gridVariables, const SolutionVector& curSol, Matrix& M, xVector& rhs, xVector& pc)
    {
        std::fill( std::begin( M ), std::end( M ), 0 );
        std::fill( std::begin( rhs ), std::end( rhs ), 0 );
        const auto& gridView = fvGridGeometry.gridView();

        const Scalar qn_inlet = 2.38e-8;  //for const flux at inlet with variable pn
        Scalar pn_inlet = 0.0;  //for const flux at inlet with variable pn

        for(const auto& element : elements(gridView))
        {
            auto curElemVolVars = localView(gridVariables.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            auto fvGeometry = localView(problem.fvGridGeometry());
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);
            elemFluxVarsCache.bind(element, fvGeometry, curElemVolVars);
            const auto eIdx = fvGridGeometry.elementMapper().index(element);  //only for const flux at inlet needed

            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    const auto& fluxVarsCache = elemFluxVarsCache[scvf];

                    for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
                    {
                        const auto vIdxSelf = scv.dofIndex();
                        const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);

                        const auto& volVars0 = curElemVolVars[scv];
                        const auto& volVars1 = curElemVolVars[1-scv.indexInElement()];
                        Scalar pcSelf;
                        Scalar pcNeighbor;
                        if(problem.spatialParams().isInletPore(vIdxSelf))
                        {
                            pcSelf = pc[vIdxSelf];
                            pcNeighbor = volVars1.capillaryPressure();
                            pc[vIdxNeighbor] = pcNeighbor;
                        }
                        else if(problem.spatialParams().isInletPore(vIdxNeighbor))
                        {
                            pcSelf = volVars0.capillaryPressure();
                            pcNeighbor = pc[vIdxNeighbor];
                            pc[vIdxSelf] = pcSelf;
                        }
                        else
                        {
                            pcSelf = volVars0.capillaryPressure();
                            pcNeighbor = volVars1.capillaryPressure();
                            pc[vIdxSelf] = pcSelf;
                            pc[vIdxNeighbor] = pcNeighbor;
                        }

//                         Scalar viscW = (volVars0.viscosity(0) + volVars1.viscosity(0))/2;
//                         Scalar viscN = (volVars0.viscosity(1) + volVars1.viscosity(1))/2;
                        Scalar viscW = 0.001;
                        Scalar viscN = 0.0001;  //1.768e-5;//

                        Scalar Kw = fluxVarsCache.transmissibility(0)/viscW;
                        Scalar Kn = fluxVarsCache.transmissibility(1)/viscN;

                        if(getParam<bool>("Problem.ConstFlux"))
                        {
                            if(eIdx == 0)  //for const flux at inlet with variable pn
                            {
                                if(vIdxSelf == 0)
                                {
    //                                 const Scalar pnNeighbor = volVars1.pressure(1);
                                    const Scalar pnNeighbor = 1.05e5;
                                    pn_inlet = qn_inlet/Kn + pnNeighbor;
                                }
                                else
                                {
    //                                 const Scalar pnSelf = volVars0.pressure(1);
                                    const Scalar pnSelf = 1.05e5;
                                    pn_inlet = qn_inlet/Kn + pnSelf;
                                }
                            }
                        }

                        EquationSystem::fillMatrix(problem, curSol, M, rhs, Kw, Kn, pcSelf, pcNeighbor, vIdxSelf, vIdxNeighbor, pn_inlet, pc);
                    }
                }
            }
        }
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PressureSolver
{
public:

    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    template<class Matrix>
    static void solve(Matrix& M, xVector& rhs, xVector& p)
    {
//         using LinearSolver = SSORCGBackend;
        using LinearSolver = UMFPackBackend;
        auto linearSolver = std::make_shared<LinearSolver>();
        linearSolver->solve(M, p, rhs);
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PartPSolver
{
public:
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    template<class Problem, class SolutionVector>
    void partpSolver(const Problem& problem, SolutionVector& curSol, const xVector& p, const xVector& pc)
    {
        const int numVertices = p.size();
        for(int i=0; i!=numVertices; i++)
        {
            if(problem.spatialParams().isInletPore(i))
            {
                const Scalar pn_dir = problem.dirichlet(i)[0];
                curSol[i][0] = pn_dir - pc[i];
            }
            else
            {
                const Scalar sn = curSol[i][1];
                curSol[i][0] = p[i] - sn * pc[i];
            }
        }
    }


    template<class Problem,class FVGridGeometry, class GridVariables, class SolutionVector>
    Scalar pcAvg(const Problem& problem, const FVGridGeometry& fvGridGeometry, const GridVariables& gridVariables, SolutionVector& curSol, const xVector& pc, std::vector<Scalar>& pcAvg_notweighted,std::vector<Scalar>& sumAn_, std::vector<Scalar>& sumPcAn_)  //calculate average capillary pressure
    {
        Scalar sumAn = 0;
        Scalar sumPcAn = 0;
        Scalar sumPc = 0;  //for non-weighted calculation
        Scalar Req;
        const auto& gridView = fvGridGeometry.gridView();
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());

        std::vector<bool> vertexMarker(fvGridGeometry.numDofs(), false);

        for(const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                const auto spatialParams = problem.spatialParams();
                const auto vIdx = scv.dofIndex();
                Scalar Sw = 1-curSol[vIdx][1];
                const Scalar sigma = materialParams.sigma();
                Scalar poreRadius = materialParams.poreRadius();
                if(vertexMarker[vIdx] == true)
                {
                    //main terminal menisci
                    if(!curElemVolVars.gridVolVars().invaded(element) && Sw < 0.48)
                    {
                        Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                        Scalar rootTerm = 1.0 - std::pow((throatRadius*pc[vIdx]/(2*sigma)),2);
                        Scalar An_menisci;
                        if(rootTerm > 0)  //negative value if sw < swMin!
                            An_menisci = 8.0 * M_PI *(sigma/pc[vIdx])*(sigma/pc[vIdx]) *(1-std::sqrt(rootTerm));
                        else
                            An_menisci = 8.0 * M_PI *(sigma/pc[vIdx])*(sigma/pc[vIdx]);


                        sumAn = sumAn + An_menisci;
                        sumPcAn = sumPcAn + pc[vIdx] * An_menisci;
                    }
                    continue;
                }

                if(Sw < 0.48)
                {
                    Req = poreRadius * (1-std::exp(-6.83 * Sw));
                    Scalar An = 4*M_PI * Req*Req + 6*M_PI * Req * (poreRadius - Req);
                    sumAn = sumAn + An;
                    sumPcAn = sumPcAn + pc[vIdx] * An;
                    sumPc = sumPc + pc[vIdx];
//                         An_.push_back(An);
//                         pcAn_.push_back(pc[vIdx] * An);

                    //main terminal menisci
                    if(!curElemVolVars.gridVolVars().invaded(element))
                    {
                        Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                        Scalar rootTerm = 1.0 - std::pow((throatRadius*pc[vIdx]/(2*sigma)),2);
                        Scalar An_menisci;
                        if(rootTerm > 0)  //negative value if sw < swMin!!
                            An_menisci = 8.0 * M_PI *(sigma/pc[vIdx])*(sigma/pc[vIdx]) *(1-std::sqrt(rootTerm));
                        else
                            An_menisci = 8.0 * M_PI *(sigma/pc[vIdx])*(sigma/pc[vIdx]);

                        sumAn = sumAn + An_menisci;
                        sumPcAn = sumPcAn + pc[vIdx] * An_menisci;
                    }
                }
                else
                {
                    Req = poreRadius * std::cbrt((6/M_PI) * (1-Sw));
                    Scalar An = 4*M_PI * Req*Req;
                    sumAn = sumAn + An;
                    sumPcAn = sumPcAn + pc[vIdx] * An;
                    sumPc = sumPc + pc[vIdx];
                }

                vertexMarker[vIdx] = true;
            }
        }

        sumAn_.push_back(sumAn);
        sumPcAn_.push_back(sumPcAn);
        pcAvg_notweighted.push_back(sumPc/fvGridGeometry.numDofs());
        return sumPcAn/sumAn;
    }


    template<class Problem,class FVGridGeometry, class GridVariables, class SolutionVector>
    void P_Avg(const Problem& problem, const FVGridGeometry& fvGridGeometry, const GridVariables& gridVariables, SolutionVector& curSol, std::vector<Scalar>& Pw_Avg, std::vector<Scalar>& Pn_Avg)  //calculate average phase pressures
    {
        Scalar sum_pwswV = 0.0;
        Scalar sum_pnsnV = 0.0;
        Scalar sum_swV = 0.0;
        Scalar sum_snV = 0.0;
        const auto& gridView = fvGridGeometry.gridView();
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());

        std::vector<bool> Vertex_done(fvGridGeometry.numDofs(), false);

        for(const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);
            for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
            {
                const auto vIdx = scv.dofIndex();
                if(!Vertex_done[vIdx] == true)
                {
                    Vertex_done[vIdx] = true;
                    if(!problem.spatialParams().isInletPore(vIdx))
                    {
                        const auto spatialParams = problem.spatialParams();
                        const auto& volVars = curElemVolVars[scv];
                        const Scalar poreVolume = spatialParams.poreVolume(vIdx);
                        const Scalar sw = 1-curSol[vIdx][1];
                        const Scalar sn = 1-sw;
                        const Scalar pc = volVars.capillaryPressure();//MaterialLaw::sw(materialParams, poreRadius, sw);
                        const Scalar pw = curSol[vIdx][0];
                        const Scalar pn = pw + pc;

                        sum_pwswV += pw * sw * poreVolume;
                        sum_pnsnV += pn * sn * poreVolume;
                        sum_swV += sw * poreVolume;
                        sum_snV += sn * poreVolume;
                    }
                }
            }
        }
        Pw_Avg.push_back(sum_pwswV / sum_swV);
        if(sum_snV > 0.0)
            Pn_Avg.push_back(sum_pnsnV / sum_snV);
        else
            Pn_Avg.push_back(sum_pwswV / sum_swV);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class QuasiStatic
{
public:
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    template<class Problem, class GridVariables, class SolutionVector>
    void staticSolver(const Problem& problem, const GridVariables& gridVariables, SolutionVector& curSol, const Scalar& pcInlet)   //for quasi-static simulations
    {
        using GridType = Dune::FoamGrid<1, 3>;    //using GridType = Dune::FoamGrid<1, dimWorld>;
        using Element = typename GridType::template Codim<0>::Entity;
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        auto gridView = problem.fvGridGeometry().gridView();
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
//         Scalar pcEblock = 0;
        std::vector<bool> markerElement(gridView.size(0), false);
//         const auto inletIndices = getParam<std::vector<int>>("Grid.InletIndices");
        bool anyInvasion = false;

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            const auto eIdx = gridView.indexSet().index(element);
            if(markerElement[eIdx])
                continue;

            // try to find a seed from which to start the search process
            bool isInlet = false;
            bool hasNeighbor = false;
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary()) //necessary???
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(problem.spatialParams().isInletPore(vIdx))    //if(vIdx == inletIndices[0])
                            isInlet = true;
                    }
                }
            }

            for(const auto& intersection : intersections(gridView, element))
            {
                if(intersection.neighbor())
                    hasNeighbor = true;
            }

            if(!hasNeighbor)
                continue;

            if(isInlet)
            {
                curElemVolVars.bind(element, fvGeometry, curSol);
                for(const auto& scv : scvs(fvGeometry))
                {
                    const Scalar throatRadius = problem.spatialParams().throatRadius(element, curElemVolVars);
                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                    Scalar pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                    if(pcInlet >= pcE)
                        markerElement[eIdx] = true;
                    else
                        continue;
                }
                if(markerElement[eIdx] == true)
                {
                    anyInvasion = true;

                    // use iteration instead of recursion here because the recursion can get too deep
                    std::stack<Element> elementStack;
                    elementStack.push(element);
                    while(!elementStack.empty())
                    {
                        auto e = elementStack.top();
                        elementStack.pop();

                        for(const auto& intersection : intersections(gridView, e))
                        {
                            if(!intersection.boundary())
                            {
                                auto outside = intersection.outside();
                                auto nIdx = gridView.indexSet().index(outside);
                                if(!markerElement[nIdx])
                                {
                                    curElemVolVars.bind(outside, fvGeometry, curSol);
                                    for(const auto& scv : scvs(fvGeometry))
                                    {
                                        const auto vIdx = scv.dofIndex();
                                        const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);
                                        if(!problem.spatialParams().isOutletPore(vIdx) && !problem.spatialParams().isOutletPore(vIdxNeighbor))  //assumption: outlet element cannot be drained
                                        {
                                            const Scalar throatRadius = problem.spatialParams().throatRadius(outside, curElemVolVars);
                                            const auto& materialParams = problem.spatialParams().materialLawParams(outside, scv, 0);
                                            Scalar pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                                            if(pcInlet >= pcE)
                                            {
                                                markerElement[nIdx] = true;
                                                elementStack.push(outside);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(anyInvasion)
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                const auto eIdx = gridView.indexSet().index(element);
                curElemVolVars.bind(element, fvGeometry, curSol);

                if(markerElement[eIdx])
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!problem.spatialParams().isOutletPore(vIdx))
                        {
                            const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                            const Scalar poreRadius = materialParams.poreRadius();
    //                         Scalar SwMin = MaterialLaw::swMin(materialParams, poreRadius, pcInlet);  //pcEblock instead of pcInlet
                            Scalar Sw = MaterialLaw::sw(materialParams, poreRadius, pcInlet);
                            curSol[vIdx][1] = 1-Sw;
                        }
                        else
                        {
                            curSol[vIdx][1] = 0.0;
                        }
                    }
                }
            }
        }
        else
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                curElemVolVars.bind(element, fvGeometry, curSol);
                for(const auto& scv : scvs(fvGeometry))
                {
                    const auto vIdx = scv.dofIndex();
                    if(problem.spatialParams().isInletPore(vIdx))
                    {
                        const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                        const Scalar poreRadius = materialParams.poreRadius();
    //                         Scalar SwMin = MaterialLaw::swMin(materialParams, poreRadius, pcInlet);  //pcEblock instead of pcInlet
                        Scalar Sw = MaterialLaw::sw(materialParams, poreRadius, pcInlet);
                        curSol[vIdx][1] = 1-Sw;
                    }
                }
            }
        }
    }
};


}  //end namespace dumux

#endif
