// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/2p/model.hh>

#include <dumux/material/spatialparams/porenetwork/myporenetwork2p.hh>
#include <dumux/porenetworkflow/2p/gridvolumevariables.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/myh2oair.hh>

namespace Dumux
{

template<TwoPFormulation formulation>
struct MyTwoPModelTraits
{
    using Indices = TwoPIndices;

    static constexpr TwoPFormulation priVarFormulation()
    { return formulation; }

    static constexpr int numEq() { return 2; }
    static constexpr int numPhases() { return 2; }
    static constexpr int numComponents() { return 2; }

    static constexpr bool enableAdvection() { return true; }
    static constexpr bool enableMolecularDiffusion() { return false; }
    static constexpr bool enableEnergyBalance() { return false; }
};


template <class TypeTag>
class PNMOnePProblem;

namespace Properties
{
NEW_TYPE_TAG(PNMOnePProblemTypeTag, INHERITS_FROM(PNMTwoP));

// Set the problem property
SET_TYPE_PROP(PNMOnePProblemTypeTag, Problem, Dumux::PNMOnePProblem<TypeTag>);

// the fluid system
SET_PROP(PNMOnePProblemTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
//     using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar> > ;
    typedef Dumux::FluidSystems::H2OAir<Scalar, Dumux::Components::SimpleH2O<Scalar> > type;
};

// Set the grid type
SET_TYPE_PROP(PNMOnePProblemTypeTag, Grid, Dune::FoamGrid<1, 3>);

SET_TYPE_PROP(PNMOnePProblemTypeTag, ModelTraits, MyTwoPModelTraits<GET_PROP_VALUE(TypeTag, Formulation)>);

SET_TYPE_PROP(PNMOnePProblemTypeTag, SinglePhaseTransmissibility, TransmissibilityAzzamDullien<TypeTag>);

SET_PROP(PNMOnePProblemTypeTag, GridVolumeVariables)
{
private:
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Traits = PNMDefaultGridVolumeVariablesTraits<Problem, VolumeVariables, Indices>;

public:
    using type = MyPNMTwoPGridVolumeVariables<Problem, VolumeVariables,
                                            GET_PROP_VALUE(TypeTag, EnableGridVolumeVariablesCache),
                                            Traits>;
};

SET_PROP(PNMOnePProblemTypeTag, SpatialParams)
{
private:
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
public:
    using type = MyPNMTwoPSpatialParams<FVGridGeometry, Scalar, Labels,
                                      GET_PROP_VALUE(TypeTag, PoreGeometry),
                                      GET_PROP_VALUE(TypeTag, ThroatGeometry),
                                      GET_PROP_VALUE(TypeTag, ZeroPc)>;
};

}

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;

    enum { dofCodim =  dim };

public:
    template<class SpatialParams>
    PNMOnePProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(fvGridGeometry, spatialParams), eps_(1e-7)
    {
        initialPc_ = getParam<Scalar>("Problem.InitialPc");
        finalPc_ = getParam<Scalar>("Problem.FinalPc");
        numSteps_ = getParam<int>("Problem.NumSteps");
        pcEpisode_.resize(numSteps_+1);
        breakthrough_ = false;
        breakthroughSw_ = 0.0;
        pcFreeze_ = 0;

        swEquilibrium_.resize(3);
        swEquilibrium_ = {1.0, 1.0, 1.0};

        std::cout << "The following global PCs are applied: " << std::endl;
        for (int i = 0 ; i != numSteps_+1; i++)
        {
            pcEpisode_[i] = initialPc_ + i*(finalPc_ - initialPc_)/numSteps_;
            std::cout << pcEpisode_[i] << std::endl;
        }

        logfile_.open("logfile_" + this->name() + ".txt"); //for the logfile
        logfile_ <<"Logfile for: " + this->name()  << std::endl;
        logfile_ << std::left << std::setw(25) << std::setfill(' ') << std::setprecision(14) << "Time"
                 << std::left << std::setw(25) << std::setfill(' ') << "Time Step"
                 << std::left << std::setw(25) << std::setfill(' ') << "qn(DtLimit)"
                 << std::left << std::setw(25) << std::setfill(' ') << "limitPore"
                 << std::left << std::setw(25) << std::setfill(' ') << "globalPc"
                 << std::left << std::setw(25) << std::setfill(' ') << "avgPc"
                 << std::left << std::setw(25) << std::setfill(' ') << "swAvg(inletIncl)"
                 << std::left << std::setw(25) << std::setfill(' ') << "swAvg(inletExcl)"
                 << std::left << std::setw(25) << std::setfill(' ') << "PnAvg"
                 << std::left << std::setw(25) << std::setfill(' ') << "PwAvg"
                 << std::left << std::setw(25) << std::setfill(' ') << "breakthroughSw"
                 << std::endl;

        step_ = 0;
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const //define output
    {
        return true;
    }


    /*!
     * \brief Called at the end of each time step
     */
    void postTimeStep(const GridVariables& gridVariables, const FVGridGeometry &fvGridGeometry, const SolutionVector& curSol,  xVector& pc, const Scalar t, const Scalar dt, const Scalar Pn_Avg, const Scalar Pw_Avg, const Scalar qn, const int limitPore, Scalar& pc_now)
    {
        const auto avg = Averaging<TypeTag>::averagedValues(*this, gridVariables, curSol);
        std::cout << avg.sw << std::endl;

        // store the three most recent averaged saturations
        std::rotate(swEquilibrium_.rbegin(), swEquilibrium_.rbegin()+1, swEquilibrium_.rend());
        swEquilibrium_[0]= avg.sw;

        // Check for steady state and end episode
        ++pcFreeze_;  //prevents too fast increasing pc at inlet
        Scalar change;
        if(swEquilibrium_[0] > swEquilibrium_[2])   //if swAvg is increasing in drainage process (capillary pinning)
            change = 0.0;
        else
            change = std::abs(swEquilibrium_[0]-swEquilibrium_[2])/swEquilibrium_[0];
        /*const Scalar*/ pc_now = pcEpisode_[step_];
        std::cout << "global pC applied: " << pc_now << " / " << finalPc_ << " (step " << step_ << " of " << numSteps_ << ")" << std::endl;
        std::cout << "swAverage: " << swEquilibrium_[0] << " (relative shift: " << change << ")" << std::endl;

        std::vector<bool> Vertex_done(fvGridGeometry.numDofs(), false);
        Scalar V_tot = 0;
        Scalar V_w = 0;
//         pc_global.push_back(pcEpisode_[step_-1]);
        const auto& gridView = fvGridGeometry.gridView();
        for(const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);
            for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
            {
                const auto vIdx = scv.dofIndex();
                const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);
                const Scalar sw = 1 - curSol[vIdx][1];
                if(this->spatialParams().isInletPore(vIdx))  //write new pc in pc vector
                    pc[vIdx] = pcEpisode_[step_];
                else                                         //for calculation of average saturation
                {
                    if(!Vertex_done[vIdx])
                    {
                        Vertex_done[vIdx] = true;

                        V_tot += this->spatialParams().poreVolume(vIdx);
                        V_w += sw * this->spatialParams().poreVolume(vIdx);
                    }
                }
                if(breakthrough_ == false && this->spatialParams().isOutletPore(vIdxNeighbor))  //check for breakthrough
                {
                    if(sw < 1.0)
                        breakthrough_ = true;
                }
            }
        }

        if(breakthrough_ == true)
            breakthroughSw_ = V_w/V_tot;

        Scalar sumAn = 0;
        Scalar sumPcAn = 0;
        Scalar sumPc = 0;
        Scalar Req;
        Scalar pcAvg = 0;

        auto fvGeometry = localView(this->fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
        std::vector<bool> vertexMarker(this->fvGridGeometry().numDofs(), false);

        for(const auto& element : elements(this->fvGridGeometry().gridView()))   //calculation of average capillary pressure
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto vIdx = scv.dofIndex();
                if(!this->spatialParams().isInletPore(vIdx))
                {
                    const auto& materialParams = this->spatialParams().materialLawParams(element, scv, 0);
                    const auto spatialParams = this->spatialParams();
                    Scalar Sw = 1-curSol[vIdx][1];
                    const Scalar sigma = materialParams.sigma();
                    Scalar poreRadius = materialParams.poreRadius();
                    const auto& volVars = curElemVolVars[scv];
                    const Scalar pc = volVars.capillaryPressure();

                    if(vertexMarker[vIdx] == true)
                    {
                        //main terminal menisci
                        if(!curElemVolVars.gridVolVars().invaded(element) && Sw < 0.48)
                        {
                            Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                            Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                            Scalar An_menisci;
                            if(rootTerm > 0)  //negative value if sw < swMin!!
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                            else
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);


                            sumAn = sumAn + An_menisci;
                            sumPcAn = sumPcAn + pc * An_menisci;
                        }
                        continue;
                    }

                    if(Sw < 0.48)
                    {
                        Req = poreRadius * (1-std::exp(-6.83 * Sw));
                        Scalar An = 4*M_PI * Req*Req + 6*M_PI * Req * (poreRadius - Req);
                        sumAn = sumAn + An;
                        sumPcAn = sumPcAn + pc * An;
                        sumPc = sumPc + pc;

                        //main terminal menisci
                        if(!curElemVolVars.gridVolVars().invaded(element))
                        {
                            Scalar throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                            Scalar rootTerm = 1.0 - std::pow((throatRadius*(pc/(2*sigma))),2);
                            Scalar An_menisci;
                            if(rootTerm > 0)  //negative value if sw < swMin!!
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc) *(1-std::sqrt(rootTerm));
                            else
                                An_menisci = 8.0 * M_PI *(sigma/pc)*(sigma/pc);

                            sumAn = sumAn + An_menisci;
                            sumPcAn = sumPcAn + pc * An_menisci;
                        }
                    }
                    else
                    {
                        Req = poreRadius * std::cbrt((6/M_PI) * (1-Sw));
                        Scalar An = 4*M_PI * Req*Req;
                        sumAn = sumAn + An;
                        sumPcAn = sumPcAn + pc * An;
                        sumPc = sumPc + pc;
                    }
                    vertexMarker[vIdx] = true;
                }
            }
        }
        if(sumAn > 0.0)
            pcAvg = sumPcAn/sumAn;

        if(V_w/V_tot == 1.0 || pcEpisode_[step_] - pcAvg < 1e-4)  //better criterion: if average pc equals global pc
//         if(change < 1e-8 && step_ < pcEpisode_.size()-1)
        {
            if(pcFreeze_ > 9)
            {
                ++step_;
                pcFreeze_ = 0;

                sw_Avg.push_back(V_w/V_tot);  // remember swEquilibrium and pc_global
                pc_Avg.push_back(pcAvg);
            }
        }

        logfile_ << std::fixed << std::left << std::setw(25) << std::setfill(' ') << t
                               << std::left << std::setw(25) << std::setfill(' ') << dt
                               << std::left << std::setw(25) << std::setfill(' ') << qn
                               << std::left << std::setw(25) << std::setfill(' ') << limitPore
                               << std::left << std::setw(25) << std::setfill(' ') << pc_now
                               << std::left << std::setw(25) << std::setfill(' ') << pcAvg
                               << std::left << std::setw(25) << std::setfill(' ') << avg.sw
                               << std::left << std::setw(25) << std::setfill(' ') << V_w/V_tot
                               << std::left << std::setw(25) << std::setfill(' ') << Pn_Avg
                               << std::left << std::setw(25) << std::setfill(' ') << Pw_Avg
                               << std::left << std::setw(25) << std::setfill(' ') << breakthroughSw_
                               << std::endl;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(int dofIdxGlobal) const
    {
        BoundaryTypes bcTypes;
        if (this->spatialParams().isInletPore(dofIdxGlobal) || this->spatialParams().isOutletPore(dofIdxGlobal))
            bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */

    PrimaryVariables dirichlet(int dofIdxGlobal) const
    {
        PrimaryVariables values(0.0);
        if (this->spatialParams().isInletPore(dofIdxGlobal))
            values[pressureIdx] = getParam<Scalar>("Problem.pwOutlet") + pcEpisode_[step_];//getParam<Scalar>("Problem.pwOutlet");//
        else
            values[pressureIdx] = getParam<Scalar>("Problem.pwOutlet");

        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        vertex
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * units of \f$ [ \textnormal{unit of conserved quantity} / s] \f$
     * \param vertex The vertex
     * \param volVars All volume variables for the pore
     *
     * Positive values mean that mass is created,
     * negative ones mean that it vanishes.
     */
    PrimaryVariables source(const Vertex& vertex, const VolumeVariables& volVars) const
    {
        return PrimaryVariables(0);
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);
        values[pressureIdx] = 1e5;

        return values;
    }

        /*!
     * \brief Evaluate the initial invasion state of a pore throat
     *
     * Returns true for a invaded throat and false elsewise.
     */
    bool initialInvasionState(const Element &element) const
    { return false; }

        /*!
     * \brief Calls gnuplot and plots the pc-S curve
     */
    void pcSwOutput()
    {
        std::ofstream myfile;  //write pcsw values on txt file
        myfile.open ("pcsw_dynamic.txt");
        myfile << "sw, pc" << std::endl;
        for(int i = 0; i != step_; i++)
        {
            myfile << sw_Avg[i] << ", " << pc_Avg[i] << std::endl;
        }
        myfile.close();
    }

    bool verbose() const
    { return true; }

    bool simulationFinished() const
    { return step_ >= numSteps_ ; }

    // \}

private:
    Scalar eps_;
    int step_;
    Scalar initialPc_;
    Scalar finalPc_;
    int numSteps_;
    std::vector<Scalar> swEquilibrium_;
    std::vector<Scalar> pcEpisode_;
    bool breakthrough_;
    Scalar breakthroughSw_;
    Scalar pcFreeze_;  //freeze pc inlet for the first 10 time steps after increase of pc
    std::ofstream logfile_;

    std::vector<Scalar> sw_Avg;
    std::vector<Scalar> pc_global;
    std::vector<Scalar> pc_Avg;
};
} //end namespace

#endif
