// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include <config.h>

#include <ctime>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cstdlib>     //for rand()--> example values for saturation
#include <fstream>     //for .txt outputs

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dune/istl/bvector.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

 
#include <dumux/common/properties.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/io/gnuplotinterface.hh>  //for gnuplot_
#include <dumux/io/grid/porenetworkgridcreator.hh>

#include <dumux/linear/seqsolverbackend.hh>

// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/2p/model.hh>
#include <dumux/io/plotpnmmateriallaw.hh>

#include "pnm1pproblem.hh"
#include "test_pnm2pimpes_pressuresolver.hh"
#include "test_pnm2pimpes_saturationupdate.hh"
#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>  //for gnuplot_


using namespace std;


int main(int argc, char** argv) try
{
    using namespace Dumux;

    using Scalar = double;

//     using TypeTag = TTAG(PressureTypeTag);
    using TypeTag2 = TTAG(PNMOnePProblemTypeTag);

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    using GridManager = Dumux::PoreNetworkGridCreator<3>;
    GridManager gridManager;
    gridManager.init();

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag2, FVGridGeometry);
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the spatial parameters
    using SpatialParams = typename GET_PROP_TYPE(TypeTag2, SpatialParams);
    auto spatialParams = std::make_shared<SpatialParams>(fvGridGeometry, gridManager.getGridData());

    // the problem (initial and boundary conditions)
    using PressureProblem = typename GET_PROP_TYPE(TypeTag2, Problem);
    auto pressureProblem = std::make_shared<PressureProblem>(fvGridGeometry, spatialParams);

    //get number of vertices and elements
    const auto numVertices = leafGridView.size(1);
    const auto numElements = leafGridView.size(0);
    std::vector<int> vertexIndices(numVertices);
    std::vector<int> elementIndices(numElements);

    //create map containing all neighbour vertices and connecting elements of the pore bodies
    MapMaker MMaker;        //create object (MMaker) in order to call function mapMaker within template class MapMaker
    auto map = MMaker.createMap(*fvGridGeometry, vertexIndices, elementIndices);

    // Write out the host grid and the subgrids.
    Dune::VTKWriter<typename FVGridGeometry::GridView> vtkWriter(leafGridView);
    vtkWriter.addCellData(elementIndices, "eIdx");
    vtkWriter.addVertexData(vertexIndices, "vIdx");

    //write out the connections for all the pore bodies
//     ConnectionWriter CWriter;
//     CWriter.connectionWriter(map);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //create initial pseudoSolution
    using GridVariables = typename GET_PROP_TYPE(TypeTag2, GridVariables);
    auto gridVariables = std::make_shared<GridVariables>(pressureProblem, fvGridGeometry);

    Dune::BlockVector<Dune::FieldVector<double, 2> > pseudoSolution;
    pseudoSolution.resize(numVertices);

    const Scalar Sn_initial = getParam<Scalar>("Problem.SnInitial");  //initial Sn in network
    const Scalar pc_initial = getParam<Scalar>("Problem.InitialPc");  //initial pc at inlet pores
    const bool quasiStatic = getParam<bool>("Problem.QuasiStatic");  //true if quasi-static simulation
    const Scalar pc_final = getParam<Scalar>("Problem.FinalPc");  //final pc for stepwise drainage
    const int numSteps = getParam<int>("Problem.NumSteps");  //number of steps for stepwise drainage
    int pcStep = 0;
    const bool stepWiseDrainage = getParam<int>("Problem.NumSteps") > 1;
    std::vector<Scalar> pcEpisode;  //vector containing the capillary pressure steps at inlet
    pcEpisode.resize(numSteps+1);
    for (int i = 0 ; i < pcEpisode.size(); i++)
        pcEpisode[i] = pc_initial + i*(pc_final - pc_initial)/numSteps;

    Scalar pc_inlet = pcEpisode[pcStep];  //initial capillary pressure at inlet
    Scalar satChange;  //for checking saturation changes
    Scalar Sw_Avg_old = 1.0;  //initially fully saturated with water
    Scalar eps = 1e-8;  //for checking saturation changes

    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;
    xVector p, Sn, Sn_init, pc;
    p.resize(numVertices);
    Sn.resize(numVertices);
    Sn_init.resize(numVertices);
    pc.resize(numVertices);
    InitParams InitializeParams;
    InitializeParams.initParams(*pressureProblem, *fvGridGeometry, pseudoSolution, pc_inlet, p, Sn_initial, Sn_init, pc);  //apply initial conditions
//     InitializeParams.initParams_test(*pressureProblem, *fvGridGeometry, *gridVariables, pseudoSolution, pn_inlet, p, Sn_initial, Sn_init);

    gridVariables->init(pseudoSolution, pseudoSolution);
    gridVariables->update(pseudoSolution, true);    //update for pw dependent parameters

    using VtkOutputFields = typename GET_PROP_TYPE(TypeTag2, VtkOutputFields);
    PNMVtkOutputModule<TypeTag2> dumuxvtkWriter(*gridVariables, pseudoSolution, pressureProblem->name());
    VtkOutputFields::init(dumuxvtkWriter);
    dumuxvtkWriter.addField(p, "p", decltype(dumuxvtkWriter)::FieldType::vertex);
    xVector SwMin_all;
    SwMin_all.resize(numVertices);
    dumuxvtkWriter.addField(SwMin_all, "SwMin", decltype(dumuxvtkWriter)::FieldType::vertex);
    xVector SimTime_vtk;
    SimTime_vtk.resize(numVertices);
    dumuxvtkWriter.addField(SimTime_vtk, "SimTime", decltype(dumuxvtkWriter)::FieldType::vertex);
    xVector SwAvg;
    SwAvg.resize(numVertices);
    dumuxvtkWriter.addField(SwAvg, "SwAvg", decltype(dumuxvtkWriter)::FieldType::vertex);

    dumuxvtkWriter.write(0);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // get some time loop parameters
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");  //end of simulation
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    const Scalar dt_init = getParam<Scalar>("TimeLoop.DtInitial");
    const int numOutputs = getParam<int>("TimeLoop.NumOutputs");
    Scalar dt = dt_init;
    Scalar qn = 0.0;  //qn in pore dictating the time step
    int limitPore = 0;  //pore dictating the time step
    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");
    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
//     const int FreqOutput = getParam<int>("Problem.VtpOutputFrequency");

    // create the pressure matrix
    using Scalar = double;
    using MatrixBlock = typename Dune::FieldMatrix<Scalar, 1, 1>;
    using BCRSMatrix = typename Dune::BCRSMatrix<MatrixBlock>;

    // create an empty BCRS matrix with 1x1 blocks
    auto M = BCRSMatrix(numVertices, numVertices, BCRSMatrix::random);
    Dune::MatrixIndexSet occupationPattern;
    Dune::MatrixIndexSet SatOccupationPattern;
    occupationPattern.resize(numVertices, numVertices);                //size of matrix = number of Vertices
    SatOccupationPattern.resize(numVertices, numVertices);

    //set objects and vectors for timeLoop
    MatrixPattern matrixP;
    xVector rhs, SatRhs, Sw;
    rhs.resize(numVertices);     //size of right-hand side = number of Vertices
    EquationSystem EquS;
    auto SatM = BCRSMatrix(numVertices, numVertices, BCRSMatrix::random);
    SaturationUpdate SatUp;
    SatRhs.resize(numVertices);
    Sw.resize(rhs.size());
    SaturationSolver SatSolver;

    int counter = 0;
    int vtkcounter = 0;
    std::vector<Scalar> Sw_0, Sw_first, Sw_second, Sw_third, t, p_0, p_first, p_second, p_third, pw_0, pw_first, pw_second, pw_third, pn_0, pn_first, pn_second, pn_third, pc_0, pc_first, pc_second, pc_third, dtSizes, Sw_avg, ItStep, pc_avg, pcAvg_notweighted, sumAn, sumPcAn, Pw_Avg, Pn_Avg;
    std::vector<bool> markerElement(numElements, false);
    Scalar SimTime = 0;


    // time loop
    if(!quasiStatic)  //dynamic simulation
    {
        timeLoop->start(); do
        {
            counter++;
            std::cout << "+++++++++++++++++++++++++++++++++++++++++++ iteration number: " << counter << " ++++++++++++++++++++++++++++++++++++" << std::endl;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //pressure solver
            matrixP.matrixPattern(*pressureProblem, occupationPattern, map);     //get occupationPattern from template class MatrixPattern

            occupationPattern.exportIdx(M);           //exports pattern on Matrix M

            EquS.assembleSystem(*pressureProblem, *fvGridGeometry, *gridVariables, pseudoSolution, M, rhs, pc);

            PressureSolver::solve(M, rhs, p);    //call function pSolver with parameters M and rhs

            PartPSolver PartPressSolver;
            PartPressSolver.partpSolver(*pressureProblem, pseudoSolution, p, pc);  //back calculation of phase pressures

            gridVariables->update(pseudoSolution, true);

            SimTime += dt;                         //time parameters
            t.push_back(SimTime);
            dtSizes.push_back(dt);

//             Sw_0.push_back(Sw[0]);               //for output files
//             Sw_first.push_back(Sw[1]);
//             Sw_second.push_back(Sw[2]);
//             Sw_third.push_back(Sw[3]);

//             p_0.push_back(p[7]);
//             p_first.push_back(p[8]);
//             p_second.push_back(p[9]);
//             p_third.push_back(p[10]);
//             pw_0.push_back(pseudoSolution[7][0]);
//             pw_first.push_back(pseudoSolution[8][0]);
//             pw_second.push_back(pseudoSolution[9][0]);
//             pw_third.push_back(pseudoSolution[10][0]);
//             pn_0.push_back(p[7] + Sw[7] * pc[7]);
//             pn_first.push_back(p[8] + Sw[8] * pc[8]);
//             pn_second.push_back(p[9] + Sw[9] * pc[9]);
//             pn_third.push_back(p[10] + Sw[10] * pc[10]);
//             pc_0.push_back(pc[0]);
//             pc_first.push_back(pc[1]);
//             pc_second.push_back(pc[2]);
//             pc_third.push_back(pc[3]);

            Sw_avg.push_back(SatSolver.SwAvg(*pressureProblem, *fvGridGeometry, pseudoSolution));  //get average saturation
            ItStep.push_back(counter);
            for(int i = 0; i != numVertices; i++)
                SimTime_vtk[i] = SimTime;  //write SimTime in Vtk output

            PartPressSolver.P_Avg(*pressureProblem, *fvGridGeometry, *gridVariables, pseudoSolution, Pw_Avg, Pn_Avg);  //get average phase pressures

            if(SimTime/(tEnd/numOutputs) >= vtkcounter)
            {
                vtkcounter = vtkcounter+1;
                dumuxvtkWriter.write(vtkcounter);
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //saturation update
            satChange = Sw_Avg_old - SatSolver.SwAvg(*pressureProblem, *fvGridGeometry, pseudoSolution);  //check saturation changes
            Sw_Avg_old = SatSolver.SwAvg(*pressureProblem, *fvGridGeometry, pseudoSolution);   //to remember old saturation for stepwise Drainage

            pressureProblem->postTimeStep(*gridVariables, *fvGridGeometry, pseudoSolution, pc, SimTime, dt, Pn_Avg[counter-1], Pw_Avg[counter-1], qn, limitPore, pc_inlet);  //logfile; get new inlet pc
            if(satChange <= eps)
                InitializeParams.boundaryParams(*pressureProblem, *fvGridGeometry, pseudoSolution, pc, Sn_init);  //apply new pc on inlet pores

            SatUp.matrixPattern(*pressureProblem, SatOccupationPattern, map);
            SatOccupationPattern.exportIdx(SatM);
            SatUp.assembleSatSystem(*pressureProblem, *fvGridGeometry, *gridVariables, pseudoSolution, SatM, SatRhs, Sn_init, dt_init, dt, SwMin_all, qn, limitPore, pc_inlet);

            SatSolver.solve(SatM, SatRhs, Sw);
            SatSolver.getSn(pseudoSolution, Sw, Sn);  //write new values in solution vector

            gridVariables->update(pseudoSolution, true);

            // set new dt as suggested by newton solver
            timeLoop->setTimeStepSize(dt);
            // make the new solution the old solution
            gridVariables->advanceTimeStep();
            // advance to the time loop to the next step
            timeLoop->advanceTimeStep();
            // report statistics of this time step
            timeLoop->reportTimeStep();

        } while (SimTime <= tEnd && !pressureProblem->simulationFinished());
    }
    else  //for quasi-static simulations
    {
        while(counter < numSteps)
        {
            counter = counter+1;
            QuasiStatic quasiStaticSim;
            quasiStaticSim.staticSolver(*pressureProblem, *gridVariables, pseudoSolution, pc_inlet);

            gridVariables->update(pseudoSolution, true);

            const Scalar SwAvg_static = SatSolver.SwAvg(*pressureProblem, *fvGridGeometry, pseudoSolution);
            Sw_avg.push_back(SwAvg_static);  //for txt file
            for(int i = 0; i != numVertices; i++)  //for paraview
            {
                SwAvg[i] = SwAvg_static;
            }

            pc_0.push_back(pc_inlet);  //for txt file
            dumuxvtkWriter.write(counter);
            pc_inlet = pcEpisode[counter];  //pc_inlet + pc_step;
        }
    }

    //     PlotSw.plotSw(Sw_1, t_Sw);
        GnuplotInterface<Scalar> gnuplot_;
    //     gnuplot_.setXRange(0, 0.01);
    //     gnuplot_.setYRange(0, 1);

    if(!quasiStatic)  //output for dynamic simulations
    {

        if(stepWiseDrainage)
            pressureProblem->pcSwOutput();
        //Sw
//         gnuplot_.setXlabel("time [s]");
//         gnuplot_.setYlabel("Sw [-]");
//         gnuplot_.addDataSetToPlot(t, Sw_first, "Sw_1");
//         gnuplot_.plot("Sw_1");
//         gnuplot_.addDataSetToPlot(t, Sw_second, "Sw_2");
//         gnuplot_.plot("Sw_2");

        //pw and pn
    //     gnuplot_.setYRange(98000, 105000);
    //     gnuplot_.setXlabel("time [s]");
    //     gnuplot_.setYlabel("p [-]");
    //     gnuplot_.addDataSetToPlot(t, pw_first, "pw_1");
    //     gnuplot_.plot("pw_1");
    //     gnuplot_.addDataSetToPlot(t, pn_first, "pn_1");
    //     gnuplot_.plot("pn_1");

        //pc
    //     gnuplot_.setXlabel("time [s]");
    //     gnuplot_.setYlabel("pc [-]");
    // //     gnuplot_.addDataSetToPlot(t, pc_first, "pc_1");
    // //     gnuplot_.plot("pc_1");
    //     gnuplot_.addDataSetToPlot(t, pc_second, "pc_2");
    //     gnuplot_.plot("pc_2");

    //     //Sw_avg
//         gnuplot_.setXlabel("time [s]");
//         gnuplot_.setYlabel("Sw_avg [-]");
//         gnuplot_.addDataSetToPlot(t, Sw_avg, "Sw_avg");
//         gnuplot_.plot("Sw_avg");
//         std::ofstream myfile;
//         myfile.open ("avgSw.txt");
//         for(int i = 0; i != counter; i++)
//         {
//             myfile << t[i] << ", " << Sw_avg[i] << std::endl;
//         }
//         myfile.close();

//         //pressures
//         std::ofstream myfile2;
//         myfile2.open ("pressures_pore7.txt");
//         myfile2 << "t, Sw, p, pn, pw, pc" << std::endl;
//         for(int i = 0; i != counter; i++)
//         {
//             myfile2 << t[i] << ", " << Sw_0[i] << ", " << p_0[i] << ", " << pn_0[i] << ", " << pw_0[i] << ", " << pc_0[i] << std::endl;
//         }
//         myfile2.close();
//         std::ofstream myfile3;
//         myfile3.open ("pressures_pore8.txt");
//         myfile3 << "t, Sw, p, pn, pw, pc" << std::endl;
//         for(int i = 0; i != counter; i++)
//         {
//             myfile3 << t[i] << ", " << Sw_first[i] << ", " << p_first[i] << ", " << pn_first[i] << ", " << pw_first[i] << ", " << pc_first[i] << std::endl;
//         }
//         myfile3.close();
//         std::ofstream myfile4;
//         myfile4.open ("pressures_pore9.txt");
//         myfile4 << "t, Sw, p, pn, pw, pc" << std::endl;
//         for(int i = 0; i != counter; i++)
//         {
//             myfile4 << t[i] << ", " << Sw_second[i] << ", " << p_second[i] << ", " << pn_second[i] << ", " << pw_second[i] << ", " << pc_second[i] << std::endl;
//         }
//         myfile4.close();
//         std::ofstream myfile5;
//         myfile5.open ("pressures_pore10.txt");
//         myfile5 << "t, Sw, p, pn, pw, pc" << std::endl;
//         for(int i = 0; i != counter; i++)
//         {
//             myfile5 << t[i] << ", " << Sw_third[i] << ", " << p_third[i] << ", " << pn_third[i] << ", " << pw_third[i] << ", " << pc_third[i] << std::endl;
//         }
//         myfile5.close();
//
//         std::ofstream myfile6;
//         myfile6.open ("pcAvg.csv");
//         myfile6 << "t, SwAvg, pcAvg" << std::endl;
//         for(int i = 0; i != counter; i++)
//         {
//             myfile6 << t[i] << ", " << Sw_avg[i] << ", " << pc_avg[i] << std::endl;
//         }
//         myfile6.close();

//         std::ofstream myfile7;     // for cross network: get viscous and capillary terms at element after centre pore
//         myfile7.open ("viscCap.csv");
//         myfile7 << "t, viscTerm, capTerm" << std::endl;
//         for(int i = 0; i != viscTerm.size(); i++)
//         {
//             myfile7 << t[i] << ", " << viscTerm[i] << ", " << capTerm[i] << std::endl;
//         }
//         myfile7.close();
//         std::cout << t.size() << ", " << Pn_Avg.size() << ", " << Pw_Avg.size() << std::endl;
//         std::ofstream myfile8;
//         myfile8.open ("PDiff.csv");
//         myfile8 << "t, Pn_Avg, Pw_Avg" << std::endl;
//         for(int i = 0; i != counter-1; i++)
//         {
//             myfile8 << t[i] << ", " << Pn_Avg[i] << ", " << Pw_Avg[i] << std::endl;
//         }
//         myfile8.close();
//
//         //dt
//         gnuplot_.setYRange(1e-11, 1.1e-5);
//         gnuplot_.setXlabel("time [s]");
//         gnuplot_.setYlabel("dt [-]");
//         gnuplot_.addDataSetToPlot(t, dtSizes, "dt");
//         gnuplot_.plot("dt");
    }
    else  //output for quasi-static simulations
    {
        //Sw
        gnuplot_.setXlabel("Pc [Pa]");
        gnuplot_.setYlabel("Sw [-]");
        gnuplot_.addDataSetToPlot(pc_0, Sw_avg, "pcsw");
        gnuplot_.plot("pcsw");

        std::ofstream myfile2;
        myfile2.open ("pcSw.txt");
        myfile2 << "Pc, Sw" << std::endl;
        for(int i = 0; i != counter; i++)
        {
            myfile2 << pc_0[i] << ", " << Sw_avg[i] << std::endl;
        }
        myfile2.close();
    }

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
