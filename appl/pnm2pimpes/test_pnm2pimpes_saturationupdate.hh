// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief Spatial Parameters in two-phase pore-network model
 */ 

#ifndef DUMUX_PNM_SATUPDATE_HH
#define DUMUX_PNM_SATUPDATE_HH

#include <ctime>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

#include <dune/istl/bvector.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/porenetworkflow/common/advection.hh>
#include <dumux/io/gnuplotinterface.hh>

namespace Dumux{
/*!
 * \file
 * \ingroup Common
 * \brief Spatial Parameters in two-phase pore-network model
 * \tparam
 */

using Scalar = double;

class SaturationUpdate
{
public:
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;

    template<class Problem>
    void matrixPattern(const Problem& problem, Dune::MatrixIndexSet& occupationPattern, VertexMap& map)   //matrix pattern
    {
        const auto numVertices = map.size();

        for(int vertexIdx = 0; vertexIdx < numVertices; ++vertexIdx)      //iteration through map, i.e. vertices
        {
            for(const auto& neighborData : map[vertexIdx])        //iteration through all the connections of the considered vertex
            {
                occupationPattern.add(neighborData.first, vertexIdx);
                occupationPattern.add(neighborData.first, neighborData.first);

                const auto& bcTypes = problem.boundaryTypes(vertexIdx);

                if(bcTypes.isDirichlet(0))                                         //for dirichlet
                {
                    for(int n = 0; n < numVertices; n++)
                        occupationPattern.add(vertexIdx, n);
                }
                else if(vertexIdx == 1)                                            //for const flux at inlet
                {
                    for(int n = 0; n < numVertices; n++)
                        occupationPattern.add(vertexIdx, n);
                }
            }
        }
    }

    template<class Problem, class SolutionVector>
    void getTimestep(const Problem& problem, const SolutionVector& curSol, const Scalar& dt_init, Scalar& dt, const xVector& qn, Scalar& pcEblock, xVector& swMin_all, Scalar& qn_limit, int& limitPore)   //original time stepping (Joekar-Niasar et al. 2010)
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto gridView = problem.fvGridGeometry().gridView();

        Scalar dt_i = dt_init;
        dt = dt_init;
        limitPore = 0;
        const Scalar eps = 1e-6;  //truncation criterion for delta sw
        const int numVertices = qn.size();
        std::vector<bool> Vertex_done(numVertices, false);
        auto fvGeometry = localView(problem.fvGridGeometry());
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary()) //necessary???
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;

                            if(!problem.spatialParams().isInletPore(vIdx))
                            {
                                const Scalar sw = 1-curSol[vIdx][1];
                                const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                const Scalar poreRadius = materialParams.poreRadius();
                                Scalar swMin = MaterialLaw::swMin(materialParams, poreRadius, pcEblock);
                                swMin_all[vIdx] = swMin;

                                Scalar Vi = problem.spatialParams().poreVolume(vIdx);

                                if(qn[vIdx] > 0)
                                {
                                    if(sw-swMin <= eps)  //truncation criterion
                                        dt_i = Vi/qn[vIdx] * eps;  //eps for delta sw
                                    else
                                        dt_i = Vi/qn[vIdx] * (sw - swMin);
                                }
                                else if(qn[vIdx] < 0)
                                {
                                    if(1-sw <= eps)  //truncation criterion
                                        dt_i = -Vi/qn[vIdx] * eps;
                                    else
                                        dt_i = -Vi/qn[vIdx] * (1 - sw);
                                }

                                if(dt_i < dt)
                                {
                                    dt = dt_i;
                                    limitPore = vIdx;
                                }
                            }
                        }
                    }
                }
            }
        }

//         if(dt < 1e-10)  //catch for minimum time step size
//             dt = 1e-10;
        qn_limit = qn[limitPore];
        std::cout << "time step size: " << dt << std::endl;
        std::cout << "because of pore: " << limitPore << " with qn = " << qn[limitPore] << std::endl;
    }

    template<class Problem, class GridVariables, class SolutionVector>
    void getTimestep_localswMin(const Problem& problem, const GridVariables& gridVariables, const SolutionVector& curSol, const Scalar& dt_init, Scalar& dt, const xVector& qn, xVector& swMin_all, Scalar& qn_limit, int& limitPore, std::vector<Scalar>& pcBlock_local)    //time stepping with modified definition of pcBlock
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto gridView = problem.fvGridGeometry().gridView();
        auto curElemVolVars = localView(gridVariables.curGridVolVars());

        Scalar dt_i = dt_init;
        dt = dt_init;
        limitPore = 0;
        const Scalar eps = 1e-6;  //truncation criterion for delta sw
        const int numVertices = qn.size();
        std::vector<bool> Vertex_done(numVertices, false);
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto fvGeometry2 = localView(problem.fvGridGeometry());
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;
                            if(!qn[vIdx] == 0)  //only calculate for pore bodies where non-wetting fluid is present
                            {
                                if(!problem.spatialParams().isInletPore(vIdx))
                                {
                                    const Scalar sw = 1-curSol[vIdx][1];
                                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                    const Scalar poreRadius = materialParams.poreRadius();

                                    Scalar swMin = MaterialLaw::swMin(materialParams, poreRadius, pcBlock_local[vIdx]);
                                    swMin_all[vIdx] = swMin;

                                    Scalar Vi = problem.spatialParams().poreVolume(vIdx);    //8 * poreRi[vIdx] * poreRi[vIdx] * poreRi[vIdx];

                                    if(qn[vIdx] > 0)
                                    {
                                        if(sw-swMin <= eps)
                                            dt_i = Vi/qn[vIdx] * eps;  //eps for delta sw
                                        else
                                            dt_i = Vi/qn[vIdx] * (sw - swMin);
                                    }
                                    else if(qn[vIdx] < 0)
                                    {
                                        if(1-sw <= eps)
                                            dt_i = -Vi/qn[vIdx] * eps;
                                        else
                                            dt_i = -Vi/qn[vIdx] * (1 - sw);
                                    }

                                    if(dt_i < dt)
                                    {
                                        dt = dt_i;
                                        limitPore = vIdx;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        qn_limit = qn[limitPore];
        std::cout << "time step size: " << dt << std::endl;
        std::cout << "because of pore: " << limitPore << " with qn = " << qn[limitPore] << std::endl;
    }

    template<class Problem, class SolutionVector>
    void getTimestep_pc(const Problem& problem, const SolutionVector& curSol, const Scalar dt_init, Scalar& dt, const xVector& qn, Scalar& qn_limit, int& limitPore, Scalar pc_inlet)   //time stepping based on maximum relative change of local capillary pressure
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto gridView = problem.fvGridGeometry().gridView();

        Scalar dt_i = dt_init;
        dt = dt_init;
        const Scalar pc_change = 0.01;  //(delta pc)/pc
        const int numVertices = qn.size();
        std::vector<bool> Vertex_done(numVertices, false);
        auto fvGeometry = localView(problem.fvGridGeometry());

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;

                            if(!problem.spatialParams().isInletPore(vIdx))
                            {
                                if(!qn[vIdx] == 0)
                                {
                                    const Scalar sw = 1-curSol[vIdx][1];
                                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                    const Scalar poreRadius = materialParams.poreRadius();
                                    Scalar Vi = problem.spatialParams().poreVolume(vIdx);    //8 * poreRi[vIdx] * poreRi[vIdx] * poreRi[vIdx];
                                    Scalar pc = MaterialLaw::pc(materialParams, sw);
                                    Scalar sw_new;
                                    if(pc > 1e-9)
                                    {
                                        Scalar pc_new;
                                        if(qn[vIdx] > 0)
                                            pc_new = pc + pc_change*pc;
                                        else
                                            pc_new = pc - pc_change*pc;

                                        sw_new = MaterialLaw::sw(materialParams, poreRadius, pc_new);
                                    }
                                    else
                                        sw_new = 0.99;

                                    if(qn[vIdx] > 0)
                                    {
                                        dt_i = Vi/qn[vIdx] * (sw - sw_new);
                                    }
                                    else if(qn[vIdx] < 0)
                                    {
                                        dt_i = Vi/qn[vIdx] * (sw - sw_new);
                                    }

                                    if(dt_i > 1e-20 && dt_i < dt)
                                    {
                                        dt = dt_i;
                                        limitPore = vIdx;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        qn_limit = qn[limitPore];
        std::cout << "time step size: " << dt << std::endl;
        std::cout << "because of pore: " << limitPore << " with qn = " << qn[limitPore] << std::endl;
    }

    template<class Problem, class SolutionVector>
    void getTimestep_sw(const Problem& problem, const SolutionVector& curSol, const Scalar dt_init, Scalar& dt, const xVector& qn, Scalar& qn_limit, int& limitPore)   //time stepping based on maximum change of local saturation
    {
        auto gridView = problem.fvGridGeometry().gridView();

        Scalar dt_i = dt_init;
        dt = dt_init;
        const Scalar sw_change = 0.001;  //0.02
        const int numVertices = qn.size();
        std::vector<bool> Vertex_done(numVertices, false);
        auto fvGeometry = localView(problem.fvGridGeometry());

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;

                            if(!problem.spatialParams().isInletPore(vIdx))
                            {
                                if(!qn[vIdx] == 0)
                                {
                                    const Scalar sw = 1-curSol[vIdx][1];

                                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                    const Scalar poreRadius = materialParams.poreRadius();
                                    Scalar Vi = problem.spatialParams().poreVolume(vIdx);
                                    Scalar sw_new;
                                    Scalar pc_new;
                                    if(qn[vIdx] > 0)
                                        sw_new = sw - sw_change;
                                    else
                                        sw_new = sw + sw_change;

                                    if(qn[vIdx] > 0)
                                        dt_i = Vi/qn[vIdx] * (sw - sw_new);
                                    else if(qn[vIdx] < 0)
                                        dt_i = Vi/qn[vIdx] * (sw - sw_new);

                                    if(dt_i > 1e-20 && dt_i < dt)
                                    {
                                        dt = dt_i;
                                        limitPore = vIdx;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        qn_limit = qn[limitPore];
        std::cout << "time step size: " << dt << std::endl;
        std::cout << "because of pore: " << limitPore << " with qn = " << qn[limitPore] << std::endl;
    }

    template<class Problem, class SolutionVector>
    void getTimestep_pc2(const Problem& problem, const SolutionVector& curSol, const Scalar& dt_init, Scalar& dt, const xVector& qn, Scalar& qn_limit, int& limitPore)   //time stepping based on maximum change of local capillary pressure
    {
//                 std::cout << "ok" << std::endl;
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        auto gridView = problem.fvGridGeometry().gridView();

        Scalar dt_i = dt_init;
        dt = dt_init;
        const Scalar pc_change = 100.0;  //pc_new - pc
        limitPore;
        const int numVertices = qn.size();
        std::vector<bool> Vertex_done(numVertices, false);
        auto fvGeometry = localView(problem.fvGridGeometry());

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!Vertex_done[vIdx])
                        {
                            Vertex_done[vIdx] = true;

                            if(!problem.spatialParams().isInletPore(vIdx))
                            {
                                const Scalar sw = 1-curSol[vIdx][1];
                                if(sw < 1.0)
                                {
                                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                    const Scalar poreRadius = materialParams.poreRadius();
                                    Scalar Vi = problem.spatialParams().poreVolume(vIdx);    //8 * poreRi[vIdx] * poreRi[vIdx] * poreRi[vIdx];
                                    Scalar pc = MaterialLaw::pc(materialParams, sw);
                                    Scalar sw_new = MaterialLaw::sw(materialParams, poreRadius, pc+pc_change);

                                    if(qn[vIdx] > 0)
                                    {
                                        dt_i = Vi/qn[vIdx] * (sw - sw_new);
                                    }
                                    else if(qn[vIdx] < 0)
                                    {
                                        dt_i = Vi/qn[vIdx] * (sw_new - sw);
                                    }
                                }

                                if(dt_i < dt)
                                {
                                    dt = dt_i;
                                    limitPore = vIdx;
                                }
                            }
                        }
                    }
                }
            }
        }

        qn_limit = qn[limitPore];
        std::cout << "my time step size is: " << dt << std::endl;
        std::cout << "because of pore: " << limitPore  << " with qn = " << qn[limitPore] << std::endl;
    }

    template<class Problem, class GridVariables, class SolutionVector>
    std::vector<Scalar> pcBlockLocal(const Problem& problem, const GridVariables& gridVariables, const SolutionVector& curSol, Scalar pc_inlet)   //local pcBlock; minimum entry capillary pressure at respective pore bodies
    {
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        auto gridView = problem.fvGridGeometry().gridView();
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());

        std::vector<Scalar> result(curSol.size(), 1e9);

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);

            if(!curElemVolVars.gridVolVars().invaded(element))
            {
                for(const auto& scv : scvs(fvGeometry))
                {
                    const Scalar throatRadius = problem.spatialParams().throatRadius(element, curElemVolVars);
                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                    Scalar pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                    if(pcE > pc_inlet)
                        pcE = pc_inlet;
                    result[scv.dofIndex()] = std::min(result[scv.dofIndex()], pcE);
                }
            }
        }

        for(int i = 0; i < result.size(); ++i)
            if( result[i] > 9.99e8)
                result[i] = pc_inlet;

        return result;
    }

    template<class Problem, class GridVariables, class SolutionVector>
    double pcEblock(const Problem& problem, const GridVariables& gridVariables, const SolutionVector& curSol)   //original definition of pcBlock (Joekar-Niasar et al. 2010)
    {
        using GridType = Dune::FoamGrid<1, 3>;    //using GridType = Dune::FoamGrid<1, dimWorld>;
        using Element = typename GridType::template Codim<0>::Entity;
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        auto gridView = problem.fvGridGeometry().gridView();
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
        Scalar pcEblock = 0;
        std::vector<bool> markerElement(gridView.size(0), false);
        const auto inletIndices = getParam<std::vector<int>>("Grid.InletIndices");
        bool anyInvasion = false;

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            const auto eIdx = gridView.indexSet().index(element);
            if(markerElement[eIdx])
                continue;

            // try to find a seed from which to start the search process
            bool isInlet = false;
            bool hasNeighbor = false;
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary()) //necessary???
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(problem.spatialParams().isInletPore(vIdx))    //if(vIdx == inletIndices[0])
                            isInlet = true;
                    }
                }
            }

            for(const auto& intersection : intersections(gridView, element))
            {
                if(intersection.neighbor())
                    hasNeighbor = true;
            }

            if(!hasNeighbor)
                continue;

            if(isInlet)
            {
                curElemVolVars.bind(element, fvGeometry, curSol);
                if(curElemVolVars.gridVolVars().invaded(element))
                {
                    markerElement[eIdx] = true;
                    anyInvasion = true;

                    // use iteration instead of recursion here because the recursion can get too deep
                    std::stack<Element> elementStack;
                    elementStack.push(element);
                    while(!elementStack.empty())
                    {
                        auto e = elementStack.top();
                        elementStack.pop();

                        for(const auto& intersection : intersections(gridView, e))
                        {
                            if(!intersection.boundary())
                            {
                                auto outside = intersection.outside();
                                auto nIdx = gridView.indexSet().index(outside);
                                if(!markerElement[nIdx])
                                {
                                    curElemVolVars.bind(outside, fvGeometry, curSol);
                                    if(curElemVolVars.gridVolVars().invaded(outside))
                                    {
                                        markerElement[nIdx] = true;
                                        elementStack.push(outside);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(anyInvasion)
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                curElemVolVars.bind(element, fvGeometry, curSol);
                if(curElemVolVars.gridVolVars().invaded(element))   //NOT if(markerElement[eIdx])!! --> snapoff-elements!
                    continue;

                for(const auto& intersection : intersections(gridView, element))
                {
                    if(intersection.neighbor())
                    {
                        auto outside = intersection.outside();
                        auto nIdx = gridView.indexSet().index(outside);
                        if(markerElement[nIdx])
                        {
                            for(const auto& scv : scvs(fvGeometry))
                            {
                                const Scalar throatRadius = problem.spatialParams().throatRadius(element, curElemVolVars);
                                const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                Scalar pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                                if(pcEblock == 0)
                                    pcEblock = pcE;
                                else if(pcE < pcEblock)
                                    pcEblock = pcE;
                            }
                        }
                    }
                }
            }
        }
        else  //get pcEntry of inletThroats
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                for(const auto scvf : scvfs(fvGeometry))
                {
                    if(!scvf.boundary()) //necessary???
                    {
                        for(const auto& scv : scvs(fvGeometry))
                        {
                            const auto vIdx = scv.dofIndex();
                            if(problem.spatialParams().isInletPore(vIdx))
                            {
                                curElemVolVars.bind(element, fvGeometry, curSol);
                                for(const auto& scv : scvs(fvGeometry))
                                {
                                    const Scalar throatRadius = problem.spatialParams().throatRadius(element, curElemVolVars);
                                    const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                                    Scalar pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                                    if(pcEblock == 0)
                                        pcEblock = pcE;
                                    else if(pcE < pcEblock)
                                        pcEblock = pcE;
                                }
                            }
                        }
                    }
                }
            }
        }
        return pcEblock;
    }

    template<class SolutionVector, class Matrix>
    void fillMatrix(const SolutionVector& curSol, Matrix& SatM, xVector& SatRhs, xVector& qn, const Scalar Kw, const Scalar Kn,  const Scalar pcSelf, const Scalar pcNeighbor, const Scalar pnSelf, const Scalar pnNeighbor, const int vIdxSelf, const int vIdxNeighbor, const Scalar pcgradient)  //original capillary diffusion approximation (Joekar-Niasar et al. 2010)
    {
        Scalar Qtot = (Kn + Kw) * (pnSelf - pnNeighbor) - Kw * (pcSelf - pcNeighbor);
        SatRhs[vIdxSelf] += Kn/(Kw + Kn) * Qtot;

        qn[vIdxSelf] += -Kn * (pnSelf - pnNeighbor);  //for time step size determination

        SatM[vIdxSelf][vIdxSelf] -= Kw * Kn/(Kw + Kn) * pcgradient;
        SatM[vIdxSelf][vIdxNeighbor] += Kw * Kn/(Kw + Kn) * pcgradient;
    }

    template<class SolutionVector, class Matrix>
    void fillMatrix_2(const SolutionVector& curSol, Matrix& SatM, xVector& SatRhs, xVector& qn, const Scalar Kw, const Scalar Kn,  const Scalar pcSelf, const Scalar pcNeighbor, const Scalar pnSelf, const Scalar pnNeighbor, const int vIdxSelf, const int vIdxNeighbor, const Scalar pcgradientSelf, const Scalar pcgradientNeighbor)  //modified capillary diffusion approximation
    {
        Scalar swSelf = 1 - curSol[vIdxSelf][1];
        Scalar swNeighbor = 1 - curSol[vIdxNeighbor][1];
        Scalar Qtot = (Kn + Kw) * (pnSelf - pnNeighbor) - Kw * (pcSelf - pcNeighbor);
        SatRhs[vIdxSelf] += Kn/(Kw + Kn) * Qtot + Kw * Kn/(Kw + Kn) * (pcSelf - pcNeighbor - pcgradientSelf * swSelf + pcgradientNeighbor * swNeighbor);

        qn[vIdxSelf] += -Kn * (pnSelf - pnNeighbor);  //for time step size determination

        SatM[vIdxSelf][vIdxSelf] -= Kw * Kn/(Kw + Kn) * pcgradientSelf;
        SatM[vIdxSelf][vIdxNeighbor] += Kw * Kn/(Kw + Kn) * pcgradientNeighbor;
    }

    template<class Problem, class FVGridGeometry, class GridVariables, class SolutionVector, class Matrix>
    void assembleSatSystem(const Problem& problem, const FVGridGeometry& fvGridGeometry, const GridVariables& gridVariables, const SolutionVector& curSol, Matrix& SatM, xVector& SatRhs, xVector& Sn_init, const Scalar& dt_init, Scalar& dt, xVector& SwMin_all, Scalar& qn_limit, int& limitPore, Scalar& pc_inlet)
    {
        std::fill( std::begin( SatM ), std::end( SatM ), 0 );
        std::fill( std::begin( SatRhs ), std::end( SatRhs ), 0 );
        const int numVertices = fvGridGeometry.numDofs();
        xVector qn, poreRi;
        qn.resize(numVertices);
        poreRi.resize(numVertices);

        const Scalar qn_inlet = 2.38e-8;    //for const flux at inlet with variable pn

        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        const auto& gridView = fvGridGeometry.gridView();
        auto curElemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
        auto fvGeometry = localView(problem.fvGridGeometry());

        for(const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            curElemVolVars.bind(element, fvGeometry, curSol);
            elemFluxVarsCache.bind(element, fvGeometry, curElemVolVars);
            const auto eIdx = fvGridGeometry.elementMapper().index(element);

            for(const auto scvf : scvfs(fvGeometry))      //through elements
            {
                if(!scvf.boundary())
                {
                    const auto& fluxVarsCache = elemFluxVarsCache[scvf];
                    for(const auto& scv : scvs(fvGeometry))       //through nodes of element (2)
                    {
                        const auto vIdxSelf = scv.dofIndex();
                        const auto vIdxNeighbor = gridView.indexSet().subIndex(element, 1 - scv.indexInElement(), 1);
                        Scalar swSelf = 1 - curSol[vIdxSelf][1];
                        Scalar swNeighbor = 1 - curSol[vIdxNeighbor][1];

                        auto materialLawParamsNeighbor = [&]()
                        {
                            for(const auto& otherScv : scvs(fvGeometry))
                            {
                                if(otherScv.dofIndex() != vIdxSelf)
                                    return problem.spatialParams().materialLawParams(element, otherScv, 0);
                            }
                            DUNE_THROW(Dune::InvalidStateException, "No material laws found");
                        }();

                        const auto& neighborMaterialLawParams = materialLawParamsNeighbor;
                        const Scalar poreRadiusNeighbor = neighborMaterialLawParams.poreRadius();

                        const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, 0);
                        const Scalar poreRadius = materialParams.poreRadius();

                        const auto& volVars0 = curElemVolVars[scv];
                        const auto& volVars1 = curElemVolVars[1-scv.indexInElement()];
                        const Scalar pcSelf = volVars0.capillaryPressure();
                        const Scalar pcNeighbor = volVars1.capillaryPressure();
                        Scalar pnSelf = volVars0.pressure(1);
                        Scalar pnNeighbor = volVars1.pressure(1);
//                         Scalar viscW = (volVars0.viscosity(0) + volVars1.viscosity(0))/2;  //for mean viscosity
//                         Scalar viscN = (volVars0.viscosity(1) + volVars1.viscosity(1))/2;  //for mean viscosity
                        Scalar viscW = 0.001;
                        Scalar viscN = 0.0001;//1.768e-5;//
                        const Scalar Kw = fluxVarsCache.transmissibility(0)/viscW;
                        Scalar Kn = fluxVarsCache.transmissibility(1)/viscN;

                        Scalar pcgradient;  //needed for original approximation in capillary diffusion term (Joekar-Niasar et al. 2010)
                        Scalar pcgradientSelf = MaterialLaw::dpc_dsw(materialParams, poreRadius, swSelf);
                        Scalar pcgradientNeighbor = MaterialLaw::dpc_dsw(materialParams, poreRadiusNeighbor, swNeighbor);

                        if(getParam<bool>("Problem.ConstFlux"))
                        {
                            if(eIdx == 0)  //for const flux at inlet with variable pn
                            {
                                if(vIdxSelf == 0)
                                {
                                    pnNeighbor = 1.05e5;
                                    pnSelf = qn_inlet/Kn + pnNeighbor;
                                }
                                else
                                {
                                    pnSelf = 1.05e5;
                                    pnNeighbor = qn_inlet/Kn + pnSelf;
                                }
                            }
                        }

                        if(getParam<bool>("Problem.PcApprox") == 0)   //original approximation in capillary diffusion term (Joekar-Niasar et al. 2010)
                        {
                            if(pnSelf < pnNeighbor)   //original upwind definition;
//                             if(curSol[vIdxSelf][0] < curSol[vIdxNeighbor][0])  //pw;
                            {
                                pcgradient = pcgradientNeighbor;
                            }
                            else
                            {
                                pcgradient = pcgradientSelf;
                            }

                            //for mean pcgradient;
//                            pcgradient = (pcgradientSelf + pcgradientNeighbor)/2;

                            //for (pcSelf - pcNeighbor)/(swSelf - swNeighbor); without approximation of delta pc (even less capillary diffusion)
//                             if(swSelf == swNeighbor)
//                                 pcgradient = -10.7275;
//                             else
//                                 pcgradient = (pcSelf - pcNeighbor)/(swSelf - swNeighbor);
                            SaturationUpdate::fillMatrix(curSol, SatM, SatRhs, qn, Kw, Kn, pcSelf, pcNeighbor, pnSelf, pnNeighbor, vIdxSelf, vIdxNeighbor, pcgradient);
                        }
                        else if(getParam<bool>("Problem.PcApprox") == 1)  //modified approximation without upwind decision
                        {
                            SaturationUpdate::fillMatrix_2(curSol, SatM, SatRhs, qn, Kw, Kn, pcSelf, pcNeighbor, pnSelf, pnNeighbor, vIdxSelf, vIdxNeighbor, pcgradientSelf, pcgradientNeighbor);
                        }
                    }
                }
            }
        }

//         Scalar pcEblock = 0;  //for original pcBlock and original time stepping (Joekar-Niasar et al. 2010)

        if(!getParam<bool>("TimeLoop.FixDt"))   //choose time stepping approach
//             pcEblock = SaturationUpdate::pcEblock(problem, gridVariables, curSol);  //for original pcBlock and original time stepping (Joekar-Niasar et al. 2010)


//             SaturationUpdate::getTimestep(problem, curSol, dt_init, dt, qn, pcEblock, SwMin_all, qn_limit, limitPore);  //original time stepping (Joekar-Niasar et al. 2010)

//             pcBlock_local = pcBlockLocal(problem, gridVariables, curSol, pc_inlet);  //for modified definition of pcBlock (swmin calculated locally)
//             getTimestep_localswMin(problem, gridVariables, curSol, dt_init, dt, qn, SwMin_all, qn_limit, limitPore, pcBlock_local);  //swmin calculated locally

            SaturationUpdate::getTimestep_pc(problem, curSol, dt_init, dt, qn, qn_limit, limitPore, pc_inlet);  //max rel change of pc
//             SaturationUpdate::getTimestep_sw(problem, curSol, dt_init, dt, qn, qn_limit, limitPore);  //max change of sw

        for(int i=0; i != numVertices; i++)   //add storage term on main diagonal and rhs
        {
            const Scalar Sw = 1 - curSol[i][1];
            Scalar Vi = problem.spatialParams().poreVolume(i);
            SatRhs[i] += Vi * Sw/dt;
            SatM[i][i] += Vi/dt;
        }

        for(int i=0; i != numVertices; i++)   //apply boundary conditions
        {
            if(problem.spatialParams().isInletPore(i))
            {
                SatRhs[i] = 1-Sn_init[i];
                for(int j=0; j != numVertices; j++)
                    SatM[i][j] = 0;
                SatM[i][i] = 1;
            }
            if(getParam<bool>("Problem.ConstFlux"))   //for const flux with pseudo pore at inlet
            {
                if(i == 1)
                {
                    SatRhs[i] = 1-Sn_init[0];
                    for(int j=0; j != numVertices; j++)
                        SatM[i][j] = 0;
                    SatM[i][i] = 1;
                }
            }
        }
    }
};

class SaturationSolver    //definition of class PressureSolver
{
public:
    using xVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1>>;
    using VertexMap = std::vector< std::vector< std::pair<std::size_t, std::size_t> > >;

    template<class Matrix, class P>
    static void solve(Matrix& SatM, P& SatRhs, P& Sw)
    {
//         using LinearSolver = SSORCGBackend;
        using LinearSolver = UMFPackBackend;
        auto linearSolver = std::make_shared<LinearSolver>();
        linearSolver->solve(SatM, Sw, SatRhs);
    }


    template<class SolutionVector>
    void getSn(SolutionVector& curSol, xVector& Sw, xVector& Sn)   //write Sn in SolutionVector
    {
        const int numVertices = Sw.size();
        for(int i=0; i!=numVertices; i++)
        {
            Sn[i] = 1-Sw[i];
            curSol[i][1] = Sn[i];
        }
    }

    template<class Problem, class FVGridGeometry, class SolutionVector>
    Scalar SwAvg(const Problem& problem, const FVGridGeometry &fvGridGeometry, SolutionVector& curSol)   //calculation of average saturation (without inlet pores)
    {
        auto fvGeometry = localView(problem.fvGridGeometry());
        const int numVertices = fvGridGeometry.numDofs();
        std::vector<bool> Vertex_done(numVertices, false);
        const auto& gridView = fvGridGeometry.gridView();
        Scalar V_tot = 0;
        Scalar V_w = 0;

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(!problem.spatialParams().isInletPore(vIdx))  //averaging window without inlet pores
                        {
                            if(!Vertex_done[vIdx])
                            {
                                Vertex_done[vIdx] = true;
                                const Scalar swSelf = 1 - curSol[vIdx][1];

                                V_tot += problem.spatialParams().poreVolume(vIdx);
                                V_w += swSelf * problem.spatialParams().poreVolume(vIdx);
                            }
                        }
                    }
                }
            }
        }
        return V_w/V_tot;
    }
};

}  //end namespace dumux

#endif
