// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Global flux variable cache
 */
#ifndef DUMUX_PNM2P_MY_GLOBAL_VOLVARS_HH
#define DUMUX_PNM2P_MY_GLOBAL_VOLVARS_HH

#include <dumux/common/properties.hh>
#include <dumux/discretization/box/gridvolumevariables.hh>

namespace Dumux
{

/*!
 * \ingroup ImplicitModel
 * \brief Base class for the flux variables cache vector, we store one cache per face
 */
template<class P, class VV, bool enableGridVolVarsCache, class Traits>
class MyPNMTwoPGridVolumeVariables : public BoxGridVolumeVariables<P, VV, enableGridVolVarsCache, Traits>
{
    using ParentType = BoxGridVolumeVariables<P, VV, enableGridVolVarsCache, Traits>;
    using ThisType = MyPNMTwoPGridVolumeVariables<P, VV, enableGridVolVarsCache, Traits>;
    using Problem = typename Traits::Problem;
    using Indices = typename Traits::Indices;

    /*!
    * \brief Data which is attached to each vertex and is not only
    *        stored locally.
    */
   struct InvasionState
   {
       bool curInvaded = false; //The invasion state of a pore throat at the current Newton step (true if invaded)
       bool oldInvaded = false; //The invasion state of a pore throat of the previous Newton step (true if invaded)
       bool wasSwitched = false;
   };

public:
    using ParentType::ParentType;
    using ParentType::operator=;

    //! export the volume variables type
    using VolumeVariables = typename Traits::VolumeVariables;

    //! make it possible to query if caching is enabled
    // static constexpr bool cachingEnabled = true;

    //! export the type of the local view
    using LocalView = typename Traits::template LocalView<ThisType, enableGridVolVarsCache>;

    MyPNMTwoPGridVolumeVariables(const Problem& problem) : ParentType(problem), numInvadedNew_(0), numInvadedOld_(0)
    {
        // initialize the invasion state
        invasionState_.resize(problem.fvGridGeometry().gridView().size(0));

        for (auto&& element : elements(this->problem().fvGridGeometry().gridView()))
        {
            const auto eIdx = this->problem().fvGridGeometry().elementMapper().index(element);

            invasionState_[eIdx].curInvaded = this->problem().initialInvasionState(element);
            invasionState_[eIdx].oldInvaded = invasionState_[eIdx].curInvaded;

            // count the number of invaded throats
            if(invasionState_[eIdx].curInvaded)
            {
                ++numInvadedNew_;
                ++numInvadedOld_;
            }
        }
    }

    template<class FVGridGeometry, class SolutionVector>
    void update(const FVGridGeometry& fvGridGeometry, const SolutionVector& sol)
    {
        ParentType::update(fvGridGeometry, sol);
        updateInvasionState(sol);
    }


    template<class Element>
    bool invaded(const Element& element) const
    {
//         auto fvGeometry = localView(this->problem().fvGridGeometry());   //de-comment if inlet element shall be invaded at any time
//         auto spatialParams = this->problem().spatialParams();
//         fvGeometry.bindElement(element);
//         for(const auto& scv : scvs(fvGeometry))
//         {
//             const auto vIdx = scv.dofIndex();
//             if(spatialParams.isInletPore(vIdx))
//                 return true;
//         }
        
        const auto eIdx = this->problem().fvGridGeometry().elementMapper().index(element);
        return invasionState_[eIdx].curInvaded;
    }

    // /*!
    //  * \brief Return a local restriction of this global object
    //  *        The local object is only functional after calling its bind/bindElement method
    //  *        This is a free function that will be found by means of ADL
    //  */
    // friend inline ElementVolumeVariables localView(const MyPNMTwoPGridVolumeVariables& global)
    // { return ElementVolumeVariables(global); }


    template<class SolutionVector>
    bool updateInvasionState(const SolutionVector& sol)
    {
        bool wasSwitched = false;
        for (auto&& element : elements(this->problem().fvGridGeometry().gridView()))
        {
            // const auto eIdx = fvGridGeometry.elementMapper().index(element);

            auto fvGeometry = localView(this->problem().fvGridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(*this);
            elemVolVars.bind(element, fvGeometry,  sol);

            //Checks if invasion or snap-off occured after Newton iteration step
            if(invasionSwitch_(this->problem().fvGridGeometry(), element, elemVolVars))
                wasSwitched = true;
        }
        return wasSwitched;
    }

    bool invasionOccured() const
    {
        updateNumInvaded_();
        return numInvadedOld_ != numInvadedNew_;
    }

//  get pcEblock: find invaded throats connected to inlet, then get smallest pcEntry of neighbor throats
    template<class SolutionVector>
    double pcMin(const SolutionVector& sol)
    {
        using GridType = Dune::FoamGrid<1, 3>;    //using GridType = Dune::FoamGrid<1, dimWorld>;
        using Element = typename GridType::template Codim<0>::Entity;
        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;

        auto gridView = this->problem().fvGridGeometry().gridView();
        auto spatialParams = this->problem().spatialParams();
        auto fvGeometry = localView(this->problem().fvGridGeometry());
        auto curElemVolVars = localView(*this);
        double pcMin = 0;
        std::vector<bool> markerElement(gridView.size(0), false);
        bool anyInvasion = false;

        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            const auto eIdx = gridView.indexSet().index(element);
            if(markerElement[eIdx])
                continue;

            // try to find a seed from which to start the search process
            bool isInlet = false;
            bool hasNeighbor = false;
            for(const auto scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary()) //necessary???
                {
                    for(const auto& scv : scvs(fvGeometry))
                    {
                        const auto vIdx = scv.dofIndex();
                        if(spatialParams.isInletPore(vIdx))    //if(vIdx == inletIndices[0])
                            isInlet = true;
                    }
                }
            }

            for(const auto& intersection : intersections(gridView, element))
            {
                if(intersection.neighbor())
                    hasNeighbor = true;
            }

            if(!hasNeighbor)
                continue;

            if(isInlet)
            {
                curElemVolVars.bind(element, fvGeometry, sol);
                if(curElemVolVars.gridVolVars().invaded(element))
                {
                    markerElement[eIdx] = true;
                    anyInvasion = true;

                    // use iteration instead of recursion here because the recursion can get too deep
                    std::stack<Element> elementStack;
                    elementStack.push(element);
                    while(!elementStack.empty())
                    {
                        auto e = elementStack.top();
                        elementStack.pop();

                        for(const auto& intersection : intersections(gridView, e))
                        {
                            if(!intersection.boundary())
                            {
                                auto outside = intersection.outside();
                                auto nIdx = gridView.indexSet().index(outside);
                                if(!markerElement[nIdx])
                                {
                                    curElemVolVars.bind(outside, fvGeometry, sol);
                                    if(curElemVolVars.gridVolVars().invaded(outside))
                                    {
                                        markerElement[nIdx] = true;
                                        elementStack.push(outside);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(anyInvasion)
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                curElemVolVars.bind(element, fvGeometry, sol);
                if(curElemVolVars.gridVolVars().invaded(element))   //NOT if(markerElement[eIdx])!! --> snapoff-elements!
                    continue;

                for(const auto& intersection : intersections(gridView, element))
                {
                    if(intersection.neighbor())
                    {
                        auto outside = intersection.outside();
                        auto nIdx = gridView.indexSet().index(outside);
                        if(markerElement[nIdx])
                        {
                            for(const auto& scv : scvs(fvGeometry))
                            {
                                const double throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                                const auto& materialParams = spatialParams.materialLawParams(element, scv, 0);
                                double pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                                if(pcMin == 0)
                                    pcMin = pcE;
                                else if(pcE < pcMin)
                                    pcMin = pcE;
                            }
                        }
                    }
                }
            }
        }
        else  //get pcEntry of inletThroats
        {
            for (const auto& element : elements(gridView))
            {
                fvGeometry.bindElement(element);
                for(const auto scvf : scvfs(fvGeometry))      //TODO do in intersection loop like in sanitizeGrid()
                {
                    if(!scvf.boundary()) //necessary???
                    {
                        for(const auto& scv : scvs(fvGeometry))
                        {
                            const auto vIdx = scv.dofIndex();
                            if(spatialParams.isInletPore(vIdx))    //TODO search algorithm
                            {
                                fvGeometry.bindElement(element);
                                curElemVolVars.bind(element, fvGeometry, sol);
                                for(const auto& scv : scvs(fvGeometry))
                                {
                                    const double throatRadius = spatialParams.throatRadius(element, curElemVolVars);
                                    const auto& materialParams = spatialParams.materialLawParams(element, scv, 0);
                                    double pcE = MaterialLaw::pcEntry(materialParams, throatRadius);
                                    if(pcMin == 0)
                                        pcMin = pcE;
                                    else if(pcE < pcMin)
                                        pcMin = pcE;
                                }
                            }
                        }
                    }
                }
            }
        }
        return pcMin;
    }


private:

    void updateNumInvaded_() const
    {
        numInvadedOld_ = numInvadedNew_;
        numInvadedNew_ = 0;

        for(const auto& state : invasionState_)
        {
            if(state.curInvaded)
                ++numInvadedNew_;
        }
    }

    /*!
    * \brief The switch for determining the invasion state of a pore throat.
    *        Called at the end of each Newton step
    *
    * \param element The element
    * \param elemVolVars The element volume variables
    * \param fluxVars The fluxvariables
    */
    template<class FVGridGeometry, class Element, class ElementVolumeVariables>
    bool invasionSwitch_(const FVGridGeometry& fvGridGeometry,
                         const Element& element,
                         const ElementVolumeVariables& elemVolVars)

    {
        const auto& spatialParams = this->problem().spatialParams();
        const auto eIdx = this->problem().fvGridGeometry().elementMapper().index(element);
        bool oldInvaded = invasionState_[eIdx].curInvaded;
        bool newInvaded = oldInvaded;

        const auto wPhaseIdx = this->problem().spatialParams().template wettingPhase<typename VolumeVariables::FluidSystem>(element, elemVolVars);

        // Block non-wetting phase flux out of the outlet
        if(spatialParams.isOutletThroat(eIdx))
        {
            invasionState_[eIdx].curInvaded = false;
            return false;
        }

        //Determine whether throat gets invaded or snap-off occurs
        using Scalar = decltype(elemVolVars[0].capillaryPressure());
        const std::vector<Scalar> pc = {elemVolVars[0].capillaryPressure(), elemVolVars[1].capillaryPressure()};
//         const auto pcMin = std::min_element(pc.begin(), pc.end());  //for snapoff?
        const auto pcMax = std::max_element(pc.begin(), pc.end());
        const Scalar pcEntry = spatialParams.pcEntry(element);
        const Scalar pcSnapoff = spatialParams.pcSnapoff(element);

        if(*pcMax > pcEntry)
           newInvaded = true;
        else if(*pcMax <= pcSnapoff)  //pcMin for snapoff? but then always snapoff during first invasion steps; fully-implicit model does not invade at all
           newInvaded = false;

        invasionState_[eIdx].curInvaded = newInvaded;

        //   std::cout << "elem: " << eIdx << "max: " << *pcMax <<   " pcEntry " << pcEntry<< " , sat: " <<  elemVolVars[0].saturation(1) <<  ", " << elemVolVars[1].saturation(1) <<  std::endl;

        if(newInvaded != oldInvaded && this->problem().verbose())
        {
          const std::vector<Scalar> sw = {elemVolVars[0].saturation(wPhaseIdx), elemVolVars[1].saturation(wPhaseIdx)};
          const int scvIdx = pcMax - pc.begin();
          const int vIdx = fvGridGeometry.gridView().indexSet().subIndex(element, scvIdx, 1);
          if(!oldInvaded && newInvaded)
          {
              std::cout << "Throat " << eIdx << " was invaded from pore "  << vIdx << " :";
              std::cout << " pc: " << *pcMax;
              std::cout << ", pcEntry: " << spatialParams.pcEntry(element);
              std::cout << ", sw: " << sw[scvIdx] << std::endl;
          }
          else if(oldInvaded && !newInvaded)
          {
              std::cout << "Snap-off occured at: " << eIdx << " from pore "  << vIdx << " :";
              std::cout << " pc: " << *pcMax;
              std::cout << ", pcSnapoff: " << spatialParams.pcSnapoff(element);
              std::cout << ", sw: " << sw[scvIdx] << std::endl;
          }
          else
              DUNE_THROW(Dune::InvalidStateException, "Invalid Process ");
        }
        return oldInvaded != newInvaded;
    }


    mutable std::vector<InvasionState> invasionState_;

    mutable unsigned int numInvadedNew_;
    mutable unsigned int numInvadedOld_;

};


} // end namespace

#endif
