// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of a regularized version of the pore network
 *        capillary pressure / relative permeability  <-> saturation relation.
 */
#ifndef MY_REGULARIZED_PNM_LOCAL_RULES_HH
#define MY_REGULARIZED_PNM_LOCAL_RULES_HH

#include "myporenetworklocalrules.hh"
#include "regularizedporenetworklocalrulesparams.hh"

#include <dumux/common/spline.hh>

namespace Dumux
{

namespace Properties
{
    NEW_PROP_TAG(ZeroPc);
}
/*!\ingroup fluidmatrixinteractionslaws
 *
 * \brief Implementation of the regularized  pore network
 *        capillary pressure / relative permeability  <-> saturation relation.
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values are "regularized".
 *        This means that in stead of following the curve of the material law in these regions, some linear approximation is used.
 *        Doing this is not worse than following the material law. E.g. for very low wetting phase values the material
 *        laws predict infinite values for \f$\mathrm{p_c}\f$ which is completely unphysical. In case of very high wetting phase
 *        saturations the difference between regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *         For an example figure of the regularization: RegularizedVanGenuchten
 *
 * \see PNMLocalRules
 */
template <class ScalarT, bool useZeroPc, class ParamsT>
class MyRegularizedPNMLocalRules
{
    using PNMLocalRules = Dumux::MyPNMLocalRules<ScalarT, ParamsT>;

public:
    using Params = ParamsT;
    using Scalar = typename PNMLocalRules::Scalar;


    static Scalar pc(const Params& params, const Scalar sw)
    {
        const Scalar poreRadius = params.poreRadius();
        return pc(params, poreRadius, sw);
    }

    /*!
     * \brief A regularized pore network capillary pressure-saturation
     *        curve.
     *
     * regularized part:
     *    - low saturation:  extend the \f$\mathrm{p_c(S_w)}\f$ curve with the slope at the regularization point (i.e. no kink).
     *    - high saturation: connect the high regularization point with \f$\mathrm{\overline{S}_w =1}\f$
     *                       by a straight line (yes, there is a kink :-( ).
     *
     * For the non-regularized part:
     *
     * \copydetails PNMLocalRules::pc()
     */
    static Scalar pc(const Params &params, const Scalar poreRadius, const Scalar sw)
    {
        const Scalar lowSw = params.lowSw();
        const Scalar m = params.slopeHighSw();

        // make sure that the capillary pressure observes a
        // derivative != 0 for 'illegal' saturations. This is
        // required for example by newton solvers (if the
        // derivative is calculated numerically) in order to get the
        // saturation moving to the right direction if it
        // temporarily is in an 'illegal' range.
        if(sw == 0.0)
        {
            return 0.0;
        }
        
        if (sw < lowSw)
        {
            return PNMLocalRules::pc(params, poreRadius, lowSw) + mLow_(params, poreRadius) * (sw - lowSw);
        }

        // Use a regularization for sw > 1 where either:
        // pc is zero for sw = 1 ...
        if(useZeroPc)
        {
            if(sw <= 1.0)
                return std::min(PNMLocalRules::pc(params, poreRadius, sw), m*sw -m);
            else
                return m*(sw - 1.0);
        }
        // ... or use the value given by the "raw" curve
        else
        {
            if(sw < 1.0)
                return PNMLocalRules::pc(params, poreRadius, sw);
            else
                return PNMLocalRules::pc(params, poreRadius, 1.0) + m*(sw - 1.0);
        }

    }

     /*! \brief The minimum wetting-phase saturation of a pore body
     *
     * \copydetails PNMLocalRules::swMin()
     */
    static Scalar swMin(const Params &params, Scalar poreRadius, Scalar pcMin)
    {
         return PNMLocalRules::swMin(params, poreRadius, pcMin);
    }

     /*! \brief The wetting-phase saturation of a pore body
     *
     * \copydetails PNMLocalRules::sw()
     */
    static Scalar sw(const Params &params, const Scalar poreRadius, const Scalar pc)
    {
        // retrieve the low and the high threshold saturations for the
        // unregularized capillary pressure curve from the parameters
        const Scalar lowSw = params.lowSw();
        const Scalar pcLowSw = PNMLocalRules::pc(params, poreRadius, lowSw);
        const Scalar pcHighSw = PNMLocalRules::pc(params, poreRadius, 1.0);

        // low saturation / high pc:
        if(pc > pcLowSw)
        {
            return lowSw + 1/mLow_(params, poreRadius)*(pc - pcLowSw);
        }

        // high saturation / low pc:
        if(pc < pcHighSw)
        {
//             const Scalar m = 1.0 / PNMLocalRules::dpc_dsw(params, poreRadius, 1.0);
//             return 1.0 + m*(pc - pcHighSw);
            return 1.0;
        }

        return PNMLocalRules::sw(params, poreRadius, pc);
    }

     /*! \brief The entry capillary pressure of a pore throat
     *
     * \copydetails PNMLocalRules::pcEntry()
     */
    static Scalar pcEntry(const Params &params, const Scalar throatRadius)
    {
        return PNMLocalRules::pcEntry(params, throatRadius);
    }

     /*! \brief The snap-off capillary pressure of a pore throat
     *
     * \copydetails PNMLocalRules::pcSnapoff()
     */
    static Scalar pcSnapoff(const Params &params, const Scalar throatRadius)
    {
        return PNMLocalRules::pcSnapoff(params, throatRadius);
    }

    /*!
    * \brief The curvature in a plane perpendicular to the direction of the throat
    *
    * \copydetails PNMLocalRules::curvatureRadius()
    */
    static Scalar curvatureRadius(const Params &params, const Scalar pc)
    {
        return PNMLocalRules::curvatureRadius(params, pc);
    }

     /*! \brief The transmissibiltiy for the wetting phase
     *
     * \copydetails PNMLocalRules::kw()
     */
    static Scalar kw(const Params &params, const Scalar throatRadius, const Scalar throatLenght,
                     const Scalar pc, const bool invaded)
    {
        if(invaded)
        {
            // avoid unphysical values where kw (invaded) is bigger than kw (not invaded)
            const Scalar pcRegu = std::max(pc, pcSnapoff(params, throatRadius));
            const Scalar result = PNMLocalRules::kw(params, throatRadius, throatLenght, pcRegu, true);
            assert(result < PNMLocalRules::kw(params, throatRadius, throatLenght, pc, false));
            return result;
//             return 0.0;
        }
        else
            return PNMLocalRules::kw(params, throatRadius, throatLenght,
                                     pc, false);
    }

     /*! \brief The transmissibiltiy for the non-wetting phase
     *
     * \copydetails PNMLocalRules::kn()
     */
    static Scalar kn(const Params &params, const Scalar throatRadius, const Scalar throatLenght,
                     const Scalar pc, const bool invaded)
    {
        Scalar pcRegu = pc;
        if(invaded)
        {
            // Prevent nan values that can occur at certain situations where the throat capillary pressure
            // is smaller than a crtitical capillary pressure (based on the throat geometry) but the throat is still invaded (snap-off
            // will occur in after current Newton step). //TODO: Should this be revised?
            const Scalar pcCrit = params.sigma() * std::sqrt(4-M_PI)/throatRadius;
            if(pc <= pcCrit)
                pcRegu =  pcCrit + 1e-6;
        }
        return PNMLocalRules::kn(params, throatRadius, throatLenght,
                                 pcRegu, invaded);
    }
    
         /*!
     * \brief The partial derivative of the capillary
     *        pressure w.r.t. the effective saturation.
     *
     *
     * \param swe Effective saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     * \param poreRadius The pore body radius
     */
    static Scalar dpc_dsw(const Params &params, const Scalar poreRadius, const Scalar sw) //TODO check
    {
        const Scalar lowSw = params.lowSw();
//         const Scalar lowSw = 0.032;
        const Scalar m = params.slopeHighSw();

        // make sure that the capilary pressure observes a
        // derivative != 0 for 'illegal' saturations. This is
        // required for example by newton solvers (if the
        // derivative is calculated numerically) in order to get the
        // saturation moving to the right direction if it
        // temporarily is in an 'illegal' range.
        if (sw < lowSw)
        {
            return mLow_(params, poreRadius);
        }

        // Use a regularization for sw > 1 where either:
        // pc is zero for sw = 1 ...
        if(useZeroPc)
        {
            if(sw <= 1.0)
            {
                return PNMLocalRules::dpc_dsw(params, poreRadius, sw);
            }
            else
                return m;
        }
        // ... or use the value given by the "raw" curve
        else
        {
            if(sw < 1.0)
                return PNMLocalRules::dpc_dsw(params, poreRadius, sw);
            else
                return m;
        }

//             return PNMLocalRules::dpc_dsw(params, poreRadius, sw);
    }

    template<typename... Args>
    static Scalar krw(Args&&... args)
    {
        return 1.0;
    }

    template<typename... Args>
    static Scalar krn(Args&&... args)
    {
        return 1.0;
    }

private:

    /*!
     * \brief   The slope of the straight line used to regularize
     *          saturations below the minimum saturation.
     *
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar mLow_(const Params &params, const Scalar poreRadius)
    {
        const Scalar lowSw = params.lowSw();

        return PNMLocalRules::dpc_dsw(params, poreRadius, lowSw);
    }

    /*!
     * \brief   The slope of the straight line used to regularize
     *          saturations above the minimum saturation.
     *
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar mHigh_(const Params &params, const Scalar poreRadius)
    {
        const Scalar highSw = params.highSw();

        Scalar pcswHigh = PNMLocalRules::pc(params, poreRadius, highSw);
        return (0 - pcswHigh)/(1.0 - highSw);
    }
};

}

#endif
