// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Specification of the material parameters
 *       for the PNM constitutive relations.
 */
#ifndef DUMUX_PNM_LOCAL_RULES_PARAMS_HH
#define DUMUX_PNM_LOCAL_RULES_PARAMS_HH

#include<dumux/porenetworkflow/common/geometry.hh>


namespace Dumux
{

/*!
 * \brief Specification of the material parameters
 *       for the PNM constitutive relations.
 *
 *        \ingroup fluidmatrixinteractionsparams
 *
 */
template <class Scalar, class SpatialParameters>
class PNMLocalRulesParams
{
    friend SpatialParameters;
public:

    //TODO: make this properly!
    PNMLocalRulesParams() : alpha_(M_PI/4), beta_(1.0), sigma_(0.0725), theta_(0.0)
    {
        beta_ = Dumux::beta(alpha_, theta_);
    }

    PNMLocalRulesParams(const Scalar alpha, const Scalar beta, const Scalar sigma, const Scalar theta)
     : alpha_(alpha), beta_(beta), sigma_(sigma), theta_(theta)
    {
        beta_ = Dumux::beta(alpha_, theta_);
    }

    PNMLocalRulesParams(const Scalar p0, const Scalar p1) : alpha_(M_PI/4), beta_(1.0), sigma_(0.0725), theta_(0.0)
    {
        beta_ = Dumux::beta(alpha_, theta_);
        poreRadius_[0] = p0;
        poreRadius_[1] = p1;
        index_ = 0;
    }

    Scalar poreRadius() const
    {
        return poreRadius_[index_];
    }

     /*!
     * \brief Returns the throat's corner half angle\f$\mathrm{[-]}\f$
     */
    Scalar alpha() const
    { return alpha_; }

     /*!
     * \brief Set the resistance factor \f$\mathrm{[-]}\f$
     */
    void setAlpha(Scalar t)
    { alpha_ = t; }

     /*!
     * \brief Returns the resistance factor \f$\mathrm{[-]}\f$
     */
    Scalar beta() const
    { return beta_; }

     /*!
     * \brief Set the resistance factor \f$\mathrm{[-]}\f$
     */
    void setBeta(Scalar t)
    { beta_ = t; }

     /*!
     * \brief Returns the interfacial tension \f$\mathrm{[kg/s^2]}\f$
     */
    Scalar sigma() const
    { return sigma_; }

     /*!
     * \brief Set the interfacial tension \f$\mathrm{[kg/s^2]}\f$
     */
    void setSigma(Scalar t)
    { sigma_ = t; }

    /*!
     * \brief Returns the contact angle \f$\mathrm{[°]}\f$
     */
    Scalar theta() const
    { return theta_; }

     /*!
     * \brief Set the contact angle \f$\mathrm{[°]}\f$
     */
    void setTheta(Scalar t)
    { theta_ = t; }




private:

    void setScvIdx(int i) const
    { index_ = i; }

    Scalar alpha_ ;
    Scalar beta_ ;
    Scalar sigma_ ;
    Scalar theta_ ;
    std::array<Scalar, 2> poreRadius_;
    mutable int index_;

};
} // namespace Dumux

#endif
