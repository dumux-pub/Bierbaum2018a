// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of the transmissibility laws for throats
 */
#ifndef DUMUX_PNM_THROATTRANSMISSIBILITY_HH
#define DUMUX_PNM_THROATTRANSMISSIBILITY_HH

#include <dumux/porenetworkflow/common/geometry.hh>
#include <type_traits>


namespace Dumux
{

namespace Properties
{
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(MaterialLaw);
NEW_PROP_TAG(PoreGeometry);
NEW_PROP_TAG(ThroatGeometry);
NEW_PROP_TAG(SinglePhaseTransmissibility);
NEW_PROP_TAG(NeglectPoreFlowResistance);
}

template<class TypeTag, bool mixedGeometries>
class TransmissibilityBruus {};


template<class TypeTag>
class TransmissibilityBruus<TypeTag, false>
{
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using ThroatShapeType = std::integral_constant<Shape, GET_PROP_VALUE(TypeTag, ThroatGeometry)>;
    using PoreShapeType = std::integral_constant<Shape, GET_PROP_VALUE(TypeTag, PoreGeometry)>;

    enum { dofCodim = GridView::dimension };

    static_assert((GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::SquarePrism) ||
                  (GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::CirclePrism) ||
                  (GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::TwoPlates) ||
                  (GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::TrianglePrism), "Unsupported throat geometry");

    static_assert(!(!GET_PROP_VALUE(TypeTag, NeglectPoreFlowResistance) &&
                     GET_PROP_VALUE(TypeTag, PoreGeometry) != Shape::SquarePrism &&
                     GET_PROP_VALUE(TypeTag, PoreGeometry) != Shape::TwoPlates),
                     "Only SquarePrism and TwoPlates are supported for getting pore flow resistance");

public:

     /*!
     * \brief Returns the conductivity of a prismatic throat. The shape of the cross section is chosen on compile time.
     *        Should be the fastest option
     *
     * \param problem The problem
     * \param element The element
     * \param ElementVolumeVariables The element volume variables
     */
    static Scalar singlePhaseTransmissibility(const Problem &problem,
                                          const Element &element,
                                          const ElementVolumeVariables &elemVolVars)
    {
        if(GET_PROP_VALUE(TypeTag, NeglectPoreFlowResistance))
        {
            return 1.0 / rHydThroat(problem, element, elemVolVars);
        }
        else
            return 1.0 / (rHydThroat(problem, element, elemVolVars) + rHydPores(problem, element, elemVolVars));
    }


    static Scalar rHydThroat(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
        const auto& spatialParams = problem.spatialParams();
        return rHyd_(spatialParams.throatRadius(element, elemVolVars),
                     spatialParams.throatLength(element, elemVolVars),
                     throatAspectRatio_(problem, element, elemVolVars),
                     typename ThroatShapeType::type());
    }

    static Scalar rHydPores(const Problem &problem,
                            const Element &element,
                            const ElementVolumeVariables &elemVolVars)
    {
        const auto& spatialParams = problem.spatialParams();
        Scalar result(0.0);

        for(int scvIdx = 0; scvIdx < 2; ++scvIdx)
        {
            const int dofIdxGlobal = problem.fvGridGeometry().vertexMapper().subIndex(element, scvIdx, dofCodim);
            const Scalar poreRadius = spatialParams.poreRadius(dofIdxGlobal, elemVolVars[scvIdx]);

            // if a zero radius is assigned to the pore (e.g at boundaries), do not add any resistance
            if(poreRadius < 1e-15)
                continue;

            // the pore length equals the pore radius in this case
            result += rHyd_(poreRadius,
                            poreRadius,
                            poreAspectRatio_(problem, element, elemVolVars, scvIdx),
                            typename PoreShapeType::type());
        }

        return result;
    }

protected:

    template<Shape s>
    struct needsAspectRatio
    {
        static constexpr bool value = s == Shape::EllipsePrism ||
                                      s == Shape::RectanglePrism;
    };

    template<class T = TypeTag, typename std::enable_if<!needsAspectRatio<GET_PROP_VALUE(T, ThroatGeometry)>::value, bool>::type = 0>
    static constexpr Scalar throatAspectRatio_(const Problem &problem,
                                    const Element &element,
                                    const ElementVolumeVariables &elemVolVars)
    {
        return 1.0;
    }

    template<class T = TypeTag, typename std::enable_if<needsAspectRatio<GET_PROP_VALUE(T, ThroatGeometry)>::value, bool>::type = 0>
    static Scalar throatAspectRatio_(const Problem &problem,
                                    const Element &element,
                                    const ElementVolumeVariables &elemVolVars)
    {
        return problem.spatialParams().aspectRatio(element);
    }

    template<class T = TypeTag, typename std::enable_if<!needsAspectRatio<GET_PROP_VALUE(T, ThroatGeometry)>::value, bool>::type = 0>
    static constexpr Scalar poreAspectRatio_(const Problem &problem,
                                             const Element &element,
                                             const ElementVolumeVariables &elemVolVars,
                                             const int scvIdx)
    {
        return 1.0;
    }

    template<class T = TypeTag, typename std::enable_if<needsAspectRatio<GET_PROP_VALUE(T, ThroatGeometry)>::value, bool>::type = 0>
    static Scalar poreAspectRatio_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars,
                                   const int scvIdx)
    {
        return problem.spatialParams().aspectRatio(element, scvIdx);
    }

    static Scalar rHyd_(const Scalar radius,
                        const Scalar length,
                        const Scalar aspectRatio,
                        std::integral_constant<Shape, Shape::SquarePrism>)
    {
        const Scalar sideLength = 2.0*radius;
        return 12.0/0.422 * length * 1.0/(sideLength*sideLength*sideLength*sideLength);
    }

    static Scalar rHyd_(const Scalar radius,
                        const Scalar length,
                        const Scalar aspectRatio,
                        std::integral_constant<Shape, Shape::CirclePrism>)
    {
        return 8.0/M_PI * length * 1.0/(radius*radius*radius*radius);
    }

    static Scalar rHyd_(const Scalar radius,
                        const Scalar length,
                        const Scalar aspectRatio,
                        std::integral_constant<Shape, Shape::TrianglePrism>)
    {
        constexpr Scalar sqrt3 = std::sqrt(3.0);
        const Scalar sideLength = 6.0/sqrt3 * radius;
        return 320.0/sqrt3 * length * 1.0/(sideLength*sideLength*sideLength*sideLength);
    }

    static Scalar rHyd_(const Scalar radius,
                        const Scalar length,
                        const Scalar aspectRatio,
                        std::integral_constant<Shape, Shape::TwoPlates>)
    {
        // the distance between the two parallel plates
        const Scalar width = 2*radius;

        return 12.0/(width*width*width) * length;
    }

};

template<class TypeTag>
class TransmissibilityBruus<TypeTag, true> : public TransmissibilityBruus<TypeTag, false>
{
    using ParentType = TransmissibilityBruus<TypeTag, false>;
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    enum { dofCodim = GridView::dimension };

public:

     /*!
     * \brief Returns the conductivity of a prismatic throat. The shape of the cross section is chosen on run-time
     *        based on the shape factor of the throat. Experimental stuff (probably never required...)
     *
     * \param problem The problem
     * \param element The element
     * \param ElementVolumeVariables The element volume variables
     */
    static Scalar singlePhaseTransmissibility(const Problem &problem,
                                              const Element &element,
                                              const ElementVolumeVariables &elemVolVars)
    {
        if(GET_PROP_VALUE(TypeTag, NeglectPoreFlowResistance))
            return 1.0 / rHydThroat(problem, element, elemVolVars);
        else
            return 1.0 / (rHydThroat(problem, element, elemVolVars) + rHydPores(problem, element, elemVolVars));
    }

    static Scalar rHydThroat(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
        const auto throatShape = [&]
        {
            const auto& spatialParams = problem.spatialParams();
            const Scalar shapeFactor = spatialParams.shapeFactor(element);

            if(shapeFactor > 0.07075)
                return Shape::CirclePrism;
            else if(shapeFactor > 0.05525)
                return Shape::SquarePrism;
            else
                return Shape::TrianglePrism;
        } ();

        return ParentType::rHyd_(ParentType::throatRadius_(elemVolVars),
                                 ParentType::throatLength_(elemVolVars),
                                 ParentType::throatAspectRatio_(problem, element, elemVolVars),
                                 throatShape);
    }

    static Scalar rHydPores(const Problem &problem,
                            const Element &element,
                            const ElementVolumeVariables &elemVolVars)
    {
        const auto& spatialParams = problem.spatialParams();

        const auto poreShape = [&spatialParams, &element] (const int scvIdx)
        {
            const Scalar shapeFactor = spatialParams.shapeFactor(element, scvIdx);

            if(shapeFactor > 0.07075)
                return Shape::CirclePrism;
            else if(shapeFactor > 0.05525)
                return Shape::SquarePrism;
            else
                return Shape::TrianglePrism;
        };

        Scalar result(0.0);

        for(int scvIdx = 0; scvIdx < 2; ++scvIdx)
        {
            const int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);
            // the pore length equals the pore radius in this case
            result += ParentType::rHyd_(spatialParams.poreRadius(dofIdxGlobal, elemVolVars),
                                        spatialParams.poreRadius(dofIdxGlobal, elemVolVars),
                                        poreAspectRatio_(problem, element, elemVolVars, scvIdx),
                                        poreShape(scvIdx));
        }

        return result;
    }
};

template<class TypeTag>
class TransmissibilityAzzamDullien
{
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    static_assert((GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::SquarePrism), "Unsupported throat geometry");
    static_assert(GET_PROP_VALUE(TypeTag, NeglectPoreFlowResistance), "Pore flow resistance not supported");

public:

     /*!
     * \brief Returns the conductivity of a prismatic throat with square-shaped cross-section
     *
     * \param problem The problem
     * \param element The element
     * \param ElementVolumeVariables The element volume variables
     */
    static Scalar singlePhaseTransmissibility(const Problem &problem,
                                              const Element &element,
                                              const ElementVolumeVariables &elemVolVars)
    {
        const auto& spatialParams = problem.spatialParams();
        const Scalar throatRadius = spatialParams.throatRadius(element, elemVolVars);
        const Scalar throatLength = spatialParams.throatLength(element, elemVolVars);

        const Scalar rEff= std::sqrt(4.0/M_PI)*throatRadius ;
        return M_PI/(8.0*throatLength) *rEff*rEff*rEff*rEff ;
    }

};

template<class TypeTag>
class TransmissibilityStatoil
{

};


// forward declaration
template <class TypeTag, int numPhases>
class ThroatTransmissibilityImplementation
{};


template <class TypeTag>
using ThroatTransmissibility = ThroatTransmissibilityImplementation<TypeTag, int(GET_PROP_TYPE(TypeTag, ModelTraits)::numPhases())>;



template<class TypeTag>
class ThroatTransmissibilityImplementation<TypeTag, 1>
{
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using Implementation = typename GET_PROP_TYPE(TypeTag, SinglePhaseTransmissibility);

public:

     /*!
     * \brief Returns the conductivity of a throat
     *
     * \param problem The problem
     * \param element The element
     * \param ElementVolumeVariables The element volume variables
     */
    static Scalar transmissibility(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars,
                                   const int phaseIdx = 0)
    {
        // forward to specialized function
        return Implementation::singlePhaseTransmissibility(problem, element, elemVolVars);
    }

};


template <class TypeTag>
class ThroatTransmissibilityImplementation<TypeTag, 2>
{
    using SinglePhaseTransmissibility = typename GET_PROP_TYPE(TypeTag, SinglePhaseTransmissibility);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using MaterialLaw = typename GET_PROP_TYPE(TypeTag, SpatialParams)::MaterialLaw;
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;

public:

    static Scalar transmissibility(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars,
                                   const int phaseIdx)
    {
        const auto &spatialParams = problem.spatialParams();
        const auto &params = spatialParams.materialLawParams(element);
        const Scalar throatLength = spatialParams.throatLength(element, elemVolVars);
        const Scalar throatRadius = spatialParams.throatRadius(element, elemVolVars);

        const Scalar pc = std::max(elemVolVars[0].capillaryPressure(), elemVolVars[1].capillaryPressure());
        const bool invaded = elemVolVars.gridVolVars().invaded(element);
        const int wPhaseIdx = problem.spatialParams().template wettingPhase<typename VolumeVariables::FluidSystem>(element,  elemVolVars);

        if(phaseIdx == wPhaseIdx)
        {
/*            if(spatialParams.isInletThroat(element))   //implemented by ThomasB
                return 0.0;
            else */if(invaded)
            {
                return MaterialLaw::kw(params, throatRadius, throatLength, pc, true);
            }
            else
            {
                return SinglePhaseTransmissibility::singlePhaseTransmissibility(problem, element, elemVolVars);
            }
        }
        else
        {
            // Calculate the non-wetting phase transmissibiltiy
            // Block non-wetting phase fluxes through outlet throats
            if(spatialParams.isOutletThroat(element))
                return 0.0;
            return MaterialLaw::kn(params, throatRadius, throatLength, pc, invaded);
        }
    }
};


}

#endif // DUMUX_PNM_THROATTRANSMISSIBILITY_HH
