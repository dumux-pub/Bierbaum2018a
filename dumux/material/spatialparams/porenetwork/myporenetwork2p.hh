// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The two phase spatial parameters for pore network models.
 */
#ifndef DUMUX_MY_PNM2P_SPATIAL_PARAMS_HH
#define DUMUX_MY_PNM2P_SPATIAL_PARAMS_HH

#include <dumux/material/fluidmatrixinteractions/porenetwork/myregularizedporenetworklocalrules.hh>
#include <dumux/material/spatialparams/porenetwork/porenetworkbase.hh>
#include <dumux/porenetworkflow/common/geometry.hh>

#include<dumux/io/plotpnmmateriallaw.hh>

namespace Dumux
{


/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class FVGridGeometry, class Scalar, class Labels, Shape poreGeometry, Shape throatGeometry, bool useZeroPc = true>
class MyPNMTwoPSpatialParams
: public PNMBaseSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry,
                              MyPNMTwoPSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry> >
{
    using ThisType = MyPNMTwoPSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry, useZeroPc>;
    using ParentType = PNMBaseSpatialParams<FVGridGeometry, Scalar, Labels, poreGeometry, throatGeometry, ThisType>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using GridData = PoreNetworkGridData<typename GridView::Grid>;

    using ParamsT = RegularizedPNMLocalRulesParams<Scalar, ThisType>;

public:
     //get the material law from the property system
    using MaterialLaw = MyRegularizedPNMLocalRules<Scalar, useZeroPc, ParamsT>;
    using MaterialLawParams = typename MaterialLaw::Params;

    MyPNMTwoPSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                         std::shared_ptr<const GridData> gridData)
    : ParentType(fvGridGeometry, gridData)
    {
        setParams();
    }

    void setParams()
    {
        const int numElements = this->fvGridGeometry().gridView().size(0);
        MaterialParams_.resize(numElements);



        pcEntry_.resize(numElements);
        pcSnapoff_.resize(numElements);
        pcCrit_.resize(numElements);

        for (auto&& element : elements(this->fvGridGeometry().gridView()))
        {
            int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
            const Scalar r0 = this->poreRadius(this->fvGridGeometry().gridView().indexSet().subIndex(element, 0, 1));
            const Scalar r1 = this->poreRadius(this->fvGridGeometry().gridView().indexSet().subIndex(element, 1, 1));

            MaterialParams_[eIdx] = MaterialLawParams(r0, r1);

            // calculation of the entry capillary pressure for each throat
            pcEntry_[eIdx] =  MaterialLaw::pcEntry(MaterialParams_[eIdx], this->initialThroatRadius(eIdx));
            pcSnapoff_[eIdx] =  MaterialLaw::pcSnapoff(MaterialParams_[eIdx], this->initialThroatRadius(eIdx));

            const Scalar sigma = MaterialParams_[eIdx].sigma() ;
            pcCrit_[eIdx] = sigma * std::sqrt(4-M_PI)/this->initialThroatRadius(eIdx);
            if(pcCrit_[eIdx] > pcSnapoff_[eIdx])
                std::cout<< "WARNING: pcCrit (" <<pcCrit_[eIdx]<< ") > pcSnapoff (" <<pcSnapoff_[eIdx] << ") at element " <<eIdx <<std::endl;
        }

        // plot local rules
        plotPc();
        plotTransmissibilities();
    }

    template<class FS, class ElementVolumeVariables>
    int wettingPhase(const Element&, const ElementVolumeVariables& elemVolVars) const
    {
        return 0;
    }

    template<class FS, class ElementSolutionVector>
    int wettingPhase(const Element&, const SubControlVolume& scv, const ElementSolutionVector& elemSol) const
    {
        return 0;
    }


     /*!
     * \brief Return the element (throat) specific entry capillary pressure \f$ Pa\f$
     *
     * \param element The current element
     */
    const Scalar pcEntry (const Element& element) const
    {
       const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
       return pcEntry_[eIdx];
    }

     /*!
     * \brief Return the element (throat) specific snap-off capillary pressure \f$ Pa\f$
     *
     * \param element The current element
     */
    const Scalar pcSnapoff (const Element& element) const
    {
       const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
       return pcSnapoff_[eIdx];
    }

     /*!
     * \brief Return the element (throat) critical capillary pressure \f$ Pa\f$
     *
     * \param element The current element
     */
    const Scalar pcCrit(const Element& element) const
    {
       const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
       return pcCrit_[eIdx];
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element
     * \param phaseIdx The phase index
     */
    template<class ElementVolumeVariables>
    Scalar throatCrossSection(const Element& element, const ElementVolumeVariables& elemVolVars, const int phaseIdx) const
    {
        const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);

        const Scalar totalArea = ParentType::throatCrossSection(element, elemVolVars);
        const auto wPhaseIdx =  wettingPhase<typename ElementVolumeVariables::VolumeVariables::FluidSystem>(element, elemVolVars);
        // TODO:: account for solution depenedent spatialParams

        if(elemVolVars.gridVolVars().invaded(element))
        {
            const Scalar pc = (elemVolVars[0].capillaryPressure() + elemVolVars[1].capillaryPressure())/2;
            const auto params = MaterialParams_[eIdx];
            const Scalar r = MaterialLaw::curvatureRadius(params, pc);

            if(throatGeometry == Shape::SquarePrism)
            {
                // the area of the wettting phase in all four corners
                // TODO make more general (different wetting angles!)
                const Scalar aWetting = 4*r*r*(1 - M_PI/4);
                if(phaseIdx == wPhaseIdx)
                    return aWetting;
                else
                    return totalArea - aWetting;
            }
            else
                DUNE_THROW(Dune::InvalidStateException, " no valid throat geometry");
        }
        else
        {
            if(phaseIdx == wPhaseIdx)
                return totalArea;
            else
                return 0.0;
        }
    }

      /*!
     * \brief Returns the parameter object for the PNM material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    template<class ElementSolutionVector>
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const SubControlVolume& scv,
                                               const ElementSolutionVector& elemSol) const
    {
        const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
        auto& tmp = MaterialParams_[eIdx];
        tmp.setScvIdx(scv.indexInElement());
        return tmp;
    }

      /*!
     * \brief Returns the parameter object for the PNM material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element) const
    {
        const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
        return MaterialParams_[eIdx];
    }

     /*!
     * \brief Plots the pore scale capillary pressure-saturation curve
     */
    void plotPc() const
    {
        // Scalar poreRadius(0.0);
        // // manually assign a pore radius ...
        // try {
        //     poreRadius = getParam<Scalar>("PlotMaterialLaw.PoreRadius");
        // }
        // // ... or get the min/max value automatically
        // catch(...) {
        //     try {
        //         std::string minMax = getParam<std::string>("PlotMaterialLaw.PoreRadius");
        //         if(minMax == "Min")
        //             poreRadius = this->minPoreRadius();
        //         else if(minMax == "Max")
        //             poreRadius = this->maxPoreRadius();
        //         else
        //             DUNE_THROW(Dune::InvalidStateException, "To plot the material law, you must either specify a fixed pore radius (> 0.0) or 'Min' / 'Max' to automatically detect the smallest/biggest pore");
        //     }
        //     catch(Dumux::ParameterException &e) {}
        // }
        //
        // if(poreRadius > 0.0)
        // {
        //     PlotLocalRules<MaterialLaw, Scalar> plot1, plot2;
        //     MaterialLawParams params;
        //     plot1.plotPc(params, poreRadius);
        //     plot2.plotSw(params, poreRadius);
        // }
    }

     /*!
     * \brief Plots the throat transmissibilities
     */
    void plotTransmissibilities() const
    {
        // Scalar minMaxthroatRadius(0.0);
        // // default values
        // Scalar throatLength(1e-5);
        // Scalar poreRadius(1e-4);
        // bool useFixedValues = false;
        //
        // // Manually assign a throat radius ...
        // try {
        //     minMaxthroatRadius = getParam<Scalar>("PlotMaterialLaw.ThroatRadius");
        //     useFixedValues = true;
        // }
        // catch(...) {}
        //
        // // ... and throat length
        // try {
        //     if(useFixedValues)
        //     {
        //         throatLength = getParam<Scalar>("PlotMaterialLaw.ThroatLength");
        //     }
        // }
        // catch(...) { DUNE_THROW(Dune::InvalidStateException, "When setting a fixed ThroatRadius, you must also set a fixed ThroatLength"); }
        //
        // // Or, get all values automatically
        // try {
        //     if(!useFixedValues)
        //     {
        //         std::string minMax = getParam<std::string>("PlotMaterialLaw.ThroatRadius");
        //
        //         if(minMax == "Min")
        //             minMaxthroatRadius = std::numeric_limits<Scalar>::max();
        //         if(minMax == "Max")
        //             minMaxthroatRadius = -std::numeric_limits<Scalar>::max();
        //
        //         // lambda function to help finding the smallest/biggest throat
        //         auto criterion = [&](const auto throatRadius){
        //             if(minMax == "Min")
        //                 return throatRadius < minMaxthroatRadius;
        //             else if(minMax == "Max")
        //                 return throatRadius > minMaxthroatRadius;
        //             else
        //                 DUNE_THROW(Dune::InvalidStateException, "To plot the material law, you must either specify a fixed throat radius (> 0.0) or 'Min' / 'Max' to automatically detect the smallest/biggest throat");
        //         };
        //
        //         // iterate over all throat and find the smallest/biggest one and the respective additional values
        //         for(const auto& element : elements(this->fvGridGeometry().gridView()))
        //         {
        //             const int eIdx = this->fvGridGeometry().gridView().indexSet().index(element);
        //             if(criterion(this->initialThroatRadius(eIdx)))
        //             {
        //                 minMaxthroatRadius = this->initialThroatRadius(eIdx);
        //                 throatLength = this->initialThroatLength(element);
        //                 const auto poreIdx1 = this->fvGridGeometry().gridView().indexSet().subIndex(element, 0, dim);
        //                 const auto poreIdx2 = this->fvGridGeometry().gridView().indexSet().subIndex(element, 1, dim);
        //                 const Scalar poreRadius1 = this->initialPoreRadius(poreIdx1);
        //                 const Scalar poreRadius2 = this->initialPoreRadius(poreIdx2);
        //                 poreRadius = std::min(poreRadius1, poreRadius2);
        //             }
        //         }
        //     }
        // }
        // catch(Dumux::ParameterException &e) {}
        //
        // // plot stuff
        // if(minMaxthroatRadius > 0.0)
        // {
        //     PlotLocalRules<TypeTag> plot;
        //     MaterialLawParams params;
        //     //TODO: Use actual values form fluidsystem
        //     const Scalar muWetting = 1e-3;
        //     const Scalar muNonWetting = 17e-6;
        //     plot.plotK(params, minMaxthroatRadius, throatLength, poreRadius, muWetting, muNonWetting, -0.3, 1.3);
        // }
    }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

private:

    std::vector<Scalar> pcEntry_;
    std::vector<Scalar> pcSnapoff_;
    std::vector<Scalar> pcCrit_;
    std::vector<MaterialLawParams> MaterialParams_;
};

} // namespace Dumux

#endif
